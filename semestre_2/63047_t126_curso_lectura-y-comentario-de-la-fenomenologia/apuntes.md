# 29 de enero

Aprobación:
  * 80% de asihttps://epublibre.org/libro/detalle/22757stencia.
  * Traer leído el texto.

*La fenomenología*
  * Una obra difícil.
  * Una obra magnífica.
  * No es cualquier obra filosófica: es una novela.
=> Es una de las obras más bellas en filosofía.

Hegel:
  * Abrupto.
  * Apasionado.
  * Más difícil de leer que Platón
=> Un viaje de descubrimiento.

*La fenomenología* se empieza en la caverna.
  * En la conciencia.
  * Al infierno.

Se asciende a la luz.
  * A la razón filosófica: transparente a sí misma.
=> Ascenso en espiral.
  * Un itinerario que empieza en un calvario.

Obra muy difícil:
  * El lenguaje. En ese tiempo el lenguaje filosófico se está
    formando en el mundo alemán.
    * Frases muy largas.
  * Parece una obra de misterio.
    * Siempre hay una sorpresa.

Recorrido:
  * Prólogo: exposición del método.
  * Conciencia sensible: de los presocráticos a Platón.
  * Percepción: a partir de Descartes.
  * Entendimiento: cambios de direcciones.
  * Razón: desde el Renacimiento.
=> Muchos saltos históricos.

Hegel quiere defender su tradición, por eso utiliza su lengua
vernácula.
  * *Sache*: atenerse a las cosas = construcción de todos mediante
    el lenguaje.
  * *La fenomenología* es una obra muy irónica.
  * Su filosofía es una filosofía de la muerte.
    * Pero según cómo se mire: puede ser de la vida.
  * Ser y la nada de Sartre y Beauvoir es Hegel.
=> La raíz del pensamiento del siglo XX está en *La fenomenología*.

Contexto del nacimiento: 1770 - 1775
  * Nacimiento del primer círculo romántico.
    * Nace Hegel.
  * Les toca un nuevo sistema educativo: todos hablaban griego.
    * Recuperación de los griegos.
    * La generación veneró al arte griego.
    * Hegel: Estado <=> Polis <=> Cosmos; 
      ciudadano <=> microcosmos: parte del cosmos.

Contexto de juventud:
  * A Hegel le agrada Herder.
    * La razón es histórica, la humanidad en proceso histórico.
    * La palabra «Espíritu» (*Geist*) viene de Herder y tiene
      referencia teológica.
  * 3 tendencias:
    1. Revolución francesa.
    2. *Wilhelm Meister* (*Los años de aprendizaje de Wilhelm Meister*) 
       de Goethe.
    3. Doctrina de la conciencia de Fichte.
  * Hegel, junto con su generación, son anarquistas.
  * Hegel regresa al Kant de la *Crítica de la razón pura*.

¿Hegel escribe políticamente o religiosamente?
  * Ambos están interconectados.
  * El Espíritu: la relación de los seres humanos entre sí,
    los muertos, los vivos, los que nacerán.

# 5 de febrero

Día asueto.

# 12 de febrero

Se va a trabajar en el periodo de la juventud de Hegel.
  * Comprender el carácter caótico de la *Fenomenología*
    * Es un itinerario hacia atrás, recoge los temas que trabajó
      en su periodo juvenil.
  * Es primordial para entender varias cuestiones de la *Fenomenología*.

¿Qué es un cristianismo positivo?
  * Aquel que se afirma en las instituciones.
  * Una religión institucionalizada: el protestantismo.
  * Jesucristo introduce el principio de autoridad.
    * No la hizo Jesús, sino sus discípulos.

«Conciencia desdichada» es la figura ejemplar de toda la *Fenomenología*.
  * Siempre intenta hacerse una nueva imagen del mundo, pero siempre fracasa.
=> Eso es la filosofía; lo ejemplifica con una metáforra cristiana.
  * La razón filosófica como el viernes santa especulativo.
  * Es una conciencia religiosa, coloca lo finito frente a lo infinitud.

El cristianismo tiene una ley interior: el amor.
  * Es lo que produce la unidad.

Interpretación del poema «Eleusis».
  * Poema místico y panteísta.
    * El referente del panteísmo es de Spinoza.
  * El título es una invocación revolucionaria.
  * Eleusis era un centro de culto a una diosa griega de la vida,
    la fertilidad y la agricultura.
    * Su mito más famoso es que su hija fue secuestrada por Hades,
      una vez al año esta hija sale y se reune con su madre.

La idea del Espíritu de Hegel tiene una correlación a la espera del
tercer reino, el del Espíritu.
  * El primero fue el de Dios y el cielo.
  * El segundo fue el de Cristo y la tierra.
  * El Espíritu hace llegar la libertad y la razón.
=> Para Kant no es realizable, es una idea, una norma.

La intuición intelectual se refiere al yo absoluto, no a la 
apercepción trascendental.
  * Hegel criticará esto como un yo empírico y limitado.
  * Para Hegel es el suicidio del ego y la unión con lo místico.

El infinito es negación, es el todo y la nada.
  * La reflexión filosófica no teme ante ello.

La *Fenomenología* parte de la incapacidad del lenguaje para ir a las
cosas mismas.

# 19 de febrero

No asistí.

# 26 de febrero

¿Qué es el espíritu del tiempo?
  * La convergencia del espíritu del mundo en un momento determinado.

Toda formación es una socialización.

El punto de partida de la *Fenomenología* es Kant.
  * ¿Cómo se entiende la filosofía como ciencia?
    * Está en *Los prolegómeros*.
=> El mundo es cultural: es antropológico.
  * Constituido con un yo transcendental.

La ciencia requiere sistema.
  * El modelo está en Spinoza.
    * Sistema axiomático.
=> No es lo que buscan los idealistas, incluido Hegel.
  * Se tratan de sistemas mecanicistas.
  * Alternativas:
    * Fichte: sistema genético.
      * Modelo orgánico.
    * Schelling: filosofía de la naturaleza.
      * Contiene teleología.
    => Kant en *La crítica de la razón pura*: la razón es un órgano.
  => Esos todos tienen las características de tener que ser:
  1. El punto de partida.
  2. Simples.
  3. Inmediatos.
  4. Determinados.
  5. Inertes.

El absoluto es un proceso de su propio devenir.
  * Es infinito.
  * No hay casualidad, sino finalidad.
=> La realidad es manifestación.
=> El absoluto es sujeto.
  * El sujeto es el único capaz de autodeterminarse.

# 5 de marzo

La *Fenomenología* es un estudio de las experiencias de la conciencia.
  - Lleva a consecuencias negativas: lo dado por verdadero es 
    concebido como falso continuamente.
  - El camino es el de la duda.
  - La dialéctica no es método, no puede ser algo externa al sujeto.
    - Es una estructura dinámica de la realidad.
    - Hegel nunca habló de tesis, antítesis y síntesis, sino Fichte.
  - Método es camino: la contrucción es por oposición.

# 12 de marzo

No vine.

# 19 de marzo

No hubo clase.

# 26 de marzo

Vacaciones.

# 2 de abril

Entrada al ámbito del entendimiento.

El problema en la percepción: un universal condicionado.
  - El universal se presentaba como cosa.
=> Entrada en la dialéctica de la unidad y la multiplicidad.
  - Crea confusión.
  - La solución es interiorizar las cualidades de la cosa.

La fuerza unifica, aquello capaz de desplegar su diferencia; es
esencialmente manifesstación.
  - Consigue la unidad de lo múltiple.
=> Entrada a la dialéctica del fenómeno y noúmeno.

La fuerza no se manifiesta, no se percibe, solo sus resultados; es
un concepto.

La filosofía no es el mundo que vivimos, sino el que reflexionamos.
  - Es una imagen invertida.

Se va de la certeza, lo más subjetivo y perceptible, a la ley.
  - Pero las leyes son el producto del entendimiento.
  - Detrás del objeto solo hay conciencia.
=> Su saber no es del mundo, sino de sí misma.

Pasos:
  1. Se ha partido de la conciencia sensible donde se supone que se 
     tiene una relación directa del objeto: singular.
    - No se puede dar cuenta de ello porque al hacerlo se transforma 
      en universal: un producto del sujeto.
    => Obliga salida para captar al objeto que tiene muchas partes:
       universal condicionado; un concepto con un único representante.
  2. Se llega a la conciencia perceptora que entiende a todo el mundo
     como cosa: síntesis entre universal y singular; un particular.
    - Mezcla determinaciones sensibles con conceptos del entendimiento:
      sustancias y accidentes.
    => Crea un conflicto entre la unidad y la multiplicidad: la 
       diferencia reside en el exterior de la cosa, y para evitar este
       problema, la diferencia pasa a residir dentro de la cosa:
       un universal incondicionado; el concepto.
  3. El entendimiento crea un juego de fuerzas: concepción orgánica
     del mundo.
    - Expresa una ley necesaria: descubre que no estaban en la 
      naturaleza, pasando de ser reglas sensibles a ser suprasensibles.
    => Llegada al universal absoluto: negación y afirmación de los
       universales condicionados e incondicionados.
  => Se descubre que la conciencia era más bien autoconciencia.
=> Se tendrá que realizar un desdoblamiento en la autoconciencia.

# 9 de abril

Falté por HDRío2018.

# 16 de abril

Tres formas de interiorización de la dialéctica del amo y el esclavo:
son socialización de la conciencia.
  1. Estoicismo.
    - Estructura de la conciencia del amo.
  2. Escepticismo.
    - Estructura de la conciencia del esclavo.
  3. Conciencia desdichada.
    - Estructura de la conciencia en síntesis.
    - Tres momentos de identificación:
      1. Mutable: judaísmo.
      2. Inmutable: cristianismo.
      3. Mutable/Inmutable: 3 momentos históricos:
        a. Fe = fervor = en sí = teoría.
        b. Manifestación = trabajo y goce relativo a Dios = para sí = praxis.
        c. Ascetismo = Iglesia = síntesis como conciencia universal.
=> Se llega a la Razón: síntesis de teoría y praxis especulativa.
=> La conciencia sale de sí.
  - En el lenguaje de Marx se consideran ideologías.

Estoicismo: solo ocuparse de sí mismo, el resto hay que aceptarlo.
=> Lo externo no es un obstáculo.
  - No preocupan los hechos, sino lo que se dice acerca de los hechos.

En el estoicismo hay «Yo = Yo».
  - Reafirmación de sí mismo.

En el escéptico todas las determinaciones pasan a ser negaciones.
  - Las reafirmaciones no son reales.

Todas las formas de la conciencia son conciencia desdichada.
  - Solo superable por la conciencia filosófica.

# 23 de abril

1. Estoicismo.
  * En sí.
2. Escepticismo.
  *  Para sí.
3. Conciencia desdichada.
  * En sí y para sí
  => Una síntesis en otras tres etapas:
    1. Mutable en sí: judaísmo.
    2. Inmutable para sí: Cristo.
    3. Autoconciencia en sí y para sí: unión real.
    => Otra síntesis con tres etapas:
      1. En sí / discípulos / certeza sensible.
      2. Para sí / manifestación / entendimiento / autoconciencia.
      3. En sí y para sí / razón especulativa / Iglesia.

Cuando Hegel habla de conciencia se refiere aun sujeto capaz de
regresar a sí mismo.
  * Y la razón se caracteriza porque se pone en paz con el mundo,
    que es igual a ponerse en paz consigo mismo.
    * Unidad entre el mundo y yo.
    * Razón como razón histórica y colectiva, pero también a nivel 
      individual.
    * Síntesis de la teoría y la *praxis*.

# 30 de abril

Conciencia: teoría, los otros son objetos.

Autoconciencia: *praxis* que trata sobre la conciencia misma.

Razón: síntesis de teoría y *praxis* como razón especulativa.
  - Estadios:
    1. Razón observante de la naturaleza.
      - Pero no se reconoce ni en la naturaleza orgánica ni inorgánica.
    2. Razón activa del individuo que se dirige a la comunidad.
      Tres formas de individualidad:
        - Placer y necesidad.
        - Corazón y delirio.
        - Virtud.
    3. *Die Sache selbst*

Espíritu: razón conciente de sí misma.

# 7 de mayo

La actuación es principalmente histórico-política.

Razón:
  * Razón observante.
  * Razón activa: unidad en el individuo singular.
    - El placer y la necesidad: *Fausto* de Goethe.
    - Ley del corazón / delirio, presunción y engreimiento: Rosseau y Schiller.
    - Virtud y el curso del mundo: *Quijote*.

Hegel:
  * Sí habla de la obra como expresión de la voluntad del individuo
    singular.
  * Sí habla del derecho de la propiedad física.
=> Jamás trata a la obra como una propiedad, porque es una 
   manifestación del espíritu, de nosotros (!).

# 14 de mayo

<!-- Exposición de mi parte sobre la eticidadad -->

* Mundo ético: actuar que deviene en su negación.
  - Corresponde a la conciencia.
=> Extrañamiento en tres momentos:
  1. Alienación del ser natural / realidad.
  2. Fe =  conocimiento directo.
  3. Ilustración = intelección.
* Mundo de la cultura: búsqueda de reconocimiento.
  - Corresponde a la autoconciencia.
  - La realidad es útil.
  - Razón parcial / cuantificadora / abstracta.
  - Dominación e instrumentalización del mundo.
  - La objetividad es lo útil.
  - Saber es poder.

# 21 de mayo

En la eticidad Estado = conjunto de leyes; más formal.
  - En la cultura Estado ya es = al poder o el desarrollo del trabajo.
    - El resultado del trabajo es la riqueza.

En Hegel, filosofía = historia de la filosofía: una identidad completa.
