# Control, moral y tecnología

\vskip 5em

\noindent Desde el nacimiento de la cibernética su fundador, Norbert
Wiener, hizo hincapié en que esta ciencia no podía dejar de lado
a los planteamientos morales --parencite(anonimo1)--. Esta 
preocupación nace de la relación de las máquinas artificiales y su 
impacto «al desarrollo económico y a la calidad de las sociedades» en
cuya desarrollo negativo de la cibernética puede provocar el 
surgimiento de «esclavos mecánicos» --parencite(anonimo1)--.

Esta mezcla entre optimismo tecnológico y preocupación moral permiten
matizar la narrativa dicotómica de la esperanza o el miedo ante las
nuevas tecnologías --parencite(hetland2012)--. En lugar de dos 
narrativas contrapuestas —la utopía y la distopía tecnológica—, habría 
una tríada cuyo punto intermedio es una actitud de control --parencite(hetland2012)--. Esta posición no 
rechaza las posibilidades innovadoras y positivas de las tecnologías
—como es el caso de la distopía que trata de evitar su difusión—;
sin embargo, esto no desemboca en un optimismo tecnológico, sino que
se invita a un análisis crítico de los peligros potenciales y, por
ello, de las medidas de control para evitarlo --parencite(hetland2012)--.

El control puede ejercerse de cuatro maneras --parencite(hetland2012)--:

1. Individual: cada persona es la responsable de marcar un límite 
   entre el uso y la regulación sobre las tecnologías que utiliza;
   es decir, autocontrol tecnológico.
2. Social: la comunidad participa para enseñar a sus integrantes las
   posibilidades y los límites del uso tecnológico, así como
   los integrantes son un aparato de vigilancia ante su empleo 
   incorrecto.
3. Técnológico: así como la tecnología puede provocar problemas, así
   también es capaz de evitarlos con puros medios tecnológicos; las
   tecnologías son usadas como instrumentos de control que evaden
   las posibles arbitrariedades de los otros modos de control; sin
   embargo, también producen una ambivalencia, ya que quienes tienen
   la capacidad de evadir este control, son también los que contemplan
   la posibilidad de generar nuevos medios de control.
4. Institucional: se trata de un control reactivo por medio de la
   creación de leyes y regulaciones que al ser quebradas, se toman
   medidas para compensar las faltas, lo cual también requiere un
   personal para hacer valer estas leyes.

\noindent Con probabilidad el padre de la cibernética hubiera estado 
de acuerdo con algún tipo de control sobre la tecnología, para así 
evitar sus empleos «inmorales». No obstante, es aquí donde también 
surge un paralelismo que no se explica por sí solo. Por un lado, se 
tiene que la problemática moral es solucionable mediante cierta clase 
de control. Por el otro, se asocia que a falta de control, el usuario 
de las tecnologías de la información serán proclives a su uso inmoral.

El «control» como medida pragmática deviene en principio moral; o bien,
la moral es reducida a una cuestión funcional, pese a que este implica
usos, costumbres, tradición y reflexión entorno a ello. Un ejemplo
claro de este ámbito reduccionista lo tenemos en la «guerra» contra
la «piratería» de sitios que fomentan el acceso a la información como
LibGen o Sci-hub. Otro ejemplo también es perceptible en el constante
cabildeo de empresas desarrolladoras de *software* propietario en 
detrimento de la adopción gubernamental de *software* libre o de
código abierto.

Desde los inicios del movimiento del *software* libre es perceptible
una narrativa que trata de visibilizar que, en el desarrollo 
tecnológico, el control muchas veces es sinónimo de «inmoralidad»
--parencite(stallman2016)--. Retomando los cuatro modos de control,
los entusiastas del *software* libre como Stallman argumentarían
su inmoralidad --parencite(stallman2016)-- debido a que

* el control individual fractura el tejido de las comunidades de 
  programadores al crear una carrera que se prolonga al infinito con 
  la intención de obtener mayores ganancias, ya que aquí el individuo
  no es mero usuario de *software*, sino también creador;
* el control social ralentiza el desarrollo tecnológico ya que implica
  que varias personas tengan que trabajar de manera separada en el
  mismo problema, en lugar de compartir la solución «libremente»;
* el control tecnológico provoca dependencia y genera mayores 
  esfuerzos para las personas con la capacidad técnica de abolir este
  control, cuyos ejemplos son perceptibles en las «guerras» contra
  el [DRM]{.versalita} o la criptográfica, en donde la complejidad 
  técnica se dispara sin un beneficio o dominio para nadie --parencite(barlow2016)--;
* el control institucional corre el peligro de desencadenar un Estado
  totalitario del tipo distópico al modo del Gran Hermano o en las
  «batallas» legislativas entre los bufetes de abogados de grandes
  industrias —como la del *software*, la del cine o la de la música—
  y sus consumidores.

\noindent Con Stallman este carácter inmoral de control no desemboca 
en una propuesta abierta a la desregulación, sino a una propuesta de 
licencia de uso cuyo control vela en pos del uso «libre» del *software* 
--parencite(stallman2016)--. El carácter moralizante en el discruso 
del *software* libre es tan frecuente que provocó la bifurcación de 
la iniciativa del código abierto.

El código abierto no tiene gran interés en la moralidad o inmoralidad
del desarrollo de *software*, sino en la practicidad que implica su
poco control. Para Raymond —el «padre» de esta iniciativa— el control
sobre la programación deriva en modelos catedráticos que lucen muy
bien al ser terminados, pero que implican un uso de recursos 
impresionante para una estructura poco flexible a cambios --parencite(raymond2016)--. 
Por ello, basado en el modelo de desarrollo de Linux, propone un 
modelo al estilo bazar donde no existen jerarquías fijas y donde los 
programadores intercambian su trabajo según sus necesidades --parencite(raymond2016)--.
Esto supone que aligera la carga de trabajo al mismo tiempo que acelera
el desarrollo tecnológico, por lo que no se cierra la puerta según
los principios éticos o políticos de cada programador. la moralidad
queda en un segundo plano y la promoción de esta iniciativa se basa en
los resultados prácticos que consigue.

Si bien esto ayuda a ver que las relaciones moralidad-control e 
inmoralidad-incontrol son un supuesto, no da una solución determinante
a este problema. Stallman hace ver la posibilidad de una relación 
inmoralidad-control en cuya conclusión yace otra propuesta de un tipo 
de moralidad-control. Raymond hace notar que el incontrol no va de la
mano con la inmoralidad, pero a su vez abandona esta discusión al no 
estar interesado en el carácter moral-inmoral del *software*. Por ello,
¿qué otras alternativas existen para pensar la relación entre control,
moral y tecnología?

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
