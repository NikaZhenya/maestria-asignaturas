# Tecnología y normatividad

\vskip -3em

## El caso del *software* libre

\vskip 5em

## Tema

\noindent Cuando se habla del desarrollo tecnológico es una concepción 
muy extendida que esta toma en cuenta el «progreso» de las ciencias, 
la tecnología, la ingeniería y las matemáticas ([CTIM]{.versalita}).
De ser así, ¿por qué los «creadores» de diversas tecnologías, 
futurólogos y escritores de ciencia ficción también se ocupan sobre el 
cómo *debería* aplicarse la tecnología para el *beneficio* de la 
sociedad, o los riesgos de no llevarlo a cabo? Este trabajo pretende 
abordar la cuestión y argumentar que los criterios normativos también 
forma parte de los principios que la tecnología toma en cuenta para su 
desarrollo o, por lo menos, para su discurso público. Como estudio de 
caso se abordará la relación que el movimiento del *software* libre 
tiene con la ética kantiana.

## Subtemas

1. *Éticas descriptiva y prescriptiva*. La constitución de normas, 
   el modo en el que *debe actuar* una persona, se origina de una
   forma de racionalidad ética diferente a la reflexión sobre cómo
   *efectivamente actúan* las personas. Este subtema sería para
   contextualizar la normatividad dentro de la tecnología acorde
   a lo que se conoce como «ética prescriptiva» en contraste con la
   «ética descriptiva».

2. *El mito de la «neutralidad ética» de la tecnología*. A través del
   análisis de los escritos de varios —sino es que todos— los autores 
   vistos en clase, se busca argumentar su similitud en cuanto que 
   existe cierto carácter ético prescriptivo en sus discursos. Con 
   esto se pretende fundar como un «mito» el que el desarrollo 
   tecnológico solo toma en cuenta el «progreso» que se da en las 
   [CTIM]{.versalita}. En su lugar, el desarrollo tecnológico también 
   contempla, desde su «núcleo», una normatividad en su obrar
   o, por lo menos, en el discurso que justifica su actuar.

3. *Todo empieza con Kant: el imperativo categórico del* software 
   *libre*. Como estudio de caso se propone analizar el movimiento
   del *software* libre, de manera específica en su primer llamado
   público y estandarte: «El manifiesto de [GNU]{.versalita}». A
   partir de ese texto se argumentará que la mención de a la «ética
   kantiana, o la Regla de Oro» no es casual, sino uno de sus 
   fundamentos normativos, al grado que es posible explicar algunos
   de los motivos del desprendimiento de la iniciativa del código 
   abierto.

\nocite{stallman2016, raymond2016, wells2013, anonimo1}

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
