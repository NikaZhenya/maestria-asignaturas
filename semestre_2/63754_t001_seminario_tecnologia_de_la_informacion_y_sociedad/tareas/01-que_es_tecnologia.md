# ¿Qué es la tecnología?

\vskip -3em

## Sobre el concepto en W. Brian Arthur y Martin Heidegger

\vskip 6em

\noindent Ambos autores comparten la opinión que las definiciones
tradicionales de la técnica son insuficientes. Para Arthur, la
tecnología vista como «una rama del conocimiento», «una aplicación de
la ciencia», «el estudio de las técnicas», «una práctica» o «una
actividad» no dice nada acerca del *logos* implícito dentro de la
palabra «tecno-logía» --parencite(arthur2009)--. Para Heidegger, la
técnica —en la versión inglesa fue traducida como «*technology*» que
a su vez viene de la palabra alemana «*technik*» cuya traducción es 
«tecnología» o «técnica» --parencite(heidegger1977)--; ni Arthur ni
Heidegger ofrecen una distinción entre ambas acepciones— como «un 
medio para unos fines» o «un hacer del hombre» no mienta lo que es la 
técnica; a saber, su *esencia* --parencite(heidegger1994)--. Arthur 
busca el *logos* y Heidegger la *esencia* de la técnica al mismo 
tiempo que ambos coinciden en que esta tiene una relación muy estrecha 
con el hombre --parencite(arthur2009,heidegger1977)--.

En Arthur, la búsqueda implica tres nociones sobre la tecnología.
La primera es la que considera más básica, en donde la tecnología
es «el medio para satisfacer propósitos humanos», así es como la
tecnología puede verse como un «método, proceso o dispositivo»; es 
decir, como «tecnología-singular» --parencite(arthur2009)--. A partir
de ahí, esta también puede entenderse como «un conjunto de prácticas
y de componentes», un tipo de «tecnología-plural» que permite la
gestación de nuevas tecnologías--parencite(arthur2009)--. Por último, 
la tecnología también puede ser «la colección de dispositivos y
las prácticas de ingeniería disponibles para una cultura», denominada
«tecnología-general» --parencite(arthur2009)--.

La tecnología-singular permite ver una clase de proceso evolutivo
en el desarrollo de la técnica en donde una tecnología es una mejora
de las tecnologías que le precedieron --parencite(arthur2009)--. Es
así como la tecnología-singular pasa a ser tecnología-plural, ya que
este proceso evolutivo implica un desarrollo no lineal, sino 
combinatorio de distintos tipos de tecnologías 
--parencite(arthur2009)--. No obstante, no todo el desarrollo
tecnológico se da de esta manera, sino que también hay tecnologías
que provienen «de la constante captura y aprovechamiento de los
fenómenos naturales» --parencite(arthur2009)--, tecnologías que no 
vienen directamente de ninguna otra tecnología, pero que tampoco 
provienen de la nada, sino de lo que el hombre tiene a disposición
gracias a su cultura, por lo cual entra a colación la tecnología-general
--parencite(arthur2009)--.

En cambio, Heidegger acepta que la noción de la técnica como medio
para la satisfación de fines humanos no es incorrecta; sin embargo,
esto no implica que sea verdadera --parencite(heidegger1994)--. Esta
es una definición instrumental y antropológica de la técnica que hace
querer dominarla --parencite(heidegger1994)--. En este deseo, el 
hombre pierde de vista la esencia de la técnica --parencite(heidegger1994)--.

Pero ¿cuál es la esencia de la técnica? No es la causalidad de lo
instrumental, que es rasgo fundamental de la técnica que permite a
esta salir de lo oculto y posibilitar la elaboración productora 
--parencite(heidegger1994)--. La técnica no es este mero medio de
salir de lo oculto, sino que pertenece a la «región del desocultamiento»,
la verdad en el sentido griego --parencite(heidegger1994)--.

Lo decisivo de la técnica no es la utilización de medios, sino su 
capacidad de «traer-ahí-delante»: es su esencia, que en la técnica
moderna se caracteriza por su provocación --parencite(heidegger1994)--.
Heidegger denomina esta provocación como «estructura de emplazamiento»
que hace ver que la técnica no es algo meramente instrumental ni su
esencia puede explicarse de modo metafísico o religoso --parencite(heidegger1994)--.

Una manera en como puede entenderse que la esencia no es algo relativo
al carácter instrumental del hombre es percibir cómo esta se concibe
como un peligro --parencite(heidegger1994)--. Si la técnica fuese
únicamente un medio para el hombre, esta no representaría un peligro
sustancial a este mismo; sin embargo, la técnica se muestra como
esta interpelación de verse a sí mismo, pero en cuyo peligro el
hombre solo ve a esta como una provocación --parencite(heidegger1994)--.
En fin, la esencia de la técnica es lo que permite cuestionar lo que 
hemos entendido por «esencia», por «hombre» y por «verdad», no es
tanto un instrumento para adueñarse, sino un medio para salir de
lo oculto a la verdad --parencite(heidegger1994)--.

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
