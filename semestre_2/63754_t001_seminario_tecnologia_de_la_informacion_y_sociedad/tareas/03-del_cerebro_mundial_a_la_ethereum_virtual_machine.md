# Del cerebro mundial a la Ethereum Virtual Machine

\vskip 5em

\noindent En una conferencia dada en 1937, H. G. Wells expone sus
ideas sobre el «cerebro mundial». Entre el asombro sobre la capacidad
humana de acortar los tiempos de comunicación y su preocupación por
la posibilidad de utilizar los medios tecnológicos para la guerra,
Wells indica algunas características que ayudan a definir al
cerebro mundial --parencite(wells2013)--:

* Una organización enciclopédica de amplitud mundial y presente de
  manera local.
* Un organismo que provea conocimiento a escuelas y universidades
  hasta el grado de modificarlas de manera sustancial.
* El acervo se constituiría de libros o, mejor aún, microfilmes y
  proyectores económicos para que cualquier persona los use.
* La institución sería «liberal» y en contra de la propaganda.

\noindent Una de las necesidades de infraestructura del cerebro mundial es la
idea de un mundo cada vez más conectado, donde las barreras 
geográficas ya no son un obstáculo --parencite(wells2013)--. Además,
el principal objetivo de este cerebro es de índole pedagógica: hacer
que los hombres estén a la altura de las exigencias de su tiempo
--parencite(wells2013)--.

Esta idea, aunque optimista, permite la asimilación con diversos 
proyectos culturales que se han posibilitado por las tecnologías
digital y del internet. Ejemplos podrían ser desde el Proyecto 
Gutenberg, pasando por la Wikipedia, hasta los motores de búsqueda
como Google o Duck Duck Go.

Sin embargo, el cerebro mundial tiene ambiciones más grandes: ser el
córtex cerebral que contenga toda la memoria y «percepción» de la
realidad --parencite(wells2013)--. En términos menos fisiológicos se
trataría de la organización mundial del pensamiento --parencite(wells2013)--.

Esto va a tono a *las intenciones* sobre dos tecnologías que se están
abriendo campo en la actualidad: la inteligencia artificial y las
cadenas de bloques.

En la inteligencia artificial ([IA]{.versalita}) existen dos 
tendencias la «débil» o «estrecha» y la «fuerte» o «general». La 
primera consiste en diseñar un sistema para que resuelva una tarea 
específica. Como ejemplo tenemos la [IA]{.versalita} que puede jugar ajedrez o go, 
o que es capaz de manejar un automóvil o mantener una conversación con 
una persona. Se le llama «estrecha» porque más allá de esa tarea 
específica, que quizá puede realizarla mejor que una persona, no puede 
hacer nada más. Incluso cuando dentro de esa misma tarea aparece una 
variable que no había sido completada, este sistema tiende a fallar; 
p. ej. la modificación del tablero de ajedrez o de go a una forma 
hexagonal. En este sentido es «débil» ya que su adaptabilidad puede 
requerir de una modificación de su código fuente por un tercero, a 
diferencia de las personas, que en mayor o menor medida por sí mismos 
pueden llevar a cabo la misma función tomando en cuenta el nuevo 
contexto. Por su carácter, este tipo de [IA]{.versalita} se enfoca más 
en obtener un alto rendimiento en una cuestión en «particular» --parencite(lucci2016)--.

Por otro lado, La [IA]{.versalita} «fuerte» o «general» que consiste 
en la creación de un sistema que tenga la capacidad de realizar tareas 
de diversa índole. Esta clase de [IA]{.versalita} es inexistente en la 
actualidad por los retos que plantea. Sin embargo, en teoría se 
visualiza con la capacidad de equiparar o de superar las capacidades 
«cognitivas» humanas. Ejemplos de esta clase de [IA]{.versalita} se 
encuentran en la ciencia ficción como HAL 9000 o la Matrix. Es 
«general» porque no está diseñada para cumplir una tarea en específico, 
sino que de manera amplia tiene el objetivo de «conocer» y «aprender». 
Y se le denomina «fuerte» ya que su índice de adaptabilidad a nuevos 
contextos se perfila al menos de manera equitativa a las capacidades 
humanas. Por sus características, este tipo de [IA]{.versalita} supone 
la construcción de una estructura «general» para la resolución de 
objetivos «particulares» --parencite(lucci2016)--.

El cerebro mundial de Wells responde a esta segunda acepción de la
[IA]{.versalita}, debido al objetivo general del «conocimiento» y
del «aprendizaje» que plantea. Sin embargo, más que fines de 
automatización o de procesamiento de la información, el cerebro 
mundial también requiere un «registro» —la «memoria»— de todo el
conocimiento humano.

Mantener un registro, en este caso de acceso público, plantea un
reto debido al peligro que conlleva su adulteración. Con el fin de
evitar su modificación malintencionado —como con fines 
propagandísticos, como apunta Wells— es necesaria una tecnología en
la que el ser humano no pueda modificar su registro. 

Una tecnología afín a esto es la cadena de bloques, la cual es una arquitectura
distribuida que realiza registros de transacciones agrupadas en 
bloques --parencite(ether2016)--. Cada uno de estos bloques contiene
una demostración matemática que la valida dentro de una secuencia, lo
que se conoce como «consenso» --parencite(ether2016)--. La 
demostración matemática es lo que de manera general se conoce como 
operación criptográfica, mientras que la arquitectura distribuida 
implica que cualquiera puede formar parte de esta read --parencite(ether2016)--.

Pese a que la idea de Wells implica un cerebro mundial «centralizado»,
la necesidad tecnológica para mantener un registro inalterable implica
la creación de una red descentralizada. Además, la asociación entre el
cerebro mundial y la cadena de bloques puede no ser claro si esta se
reduce a su primera manifestación: la criptomoneda bitcoin. Pero
puede ser más comprensible si se asocia a otra generación de cadenas
de bloques, específicamente Ethereum. 

Ethereum es una cadena de bloques abierta y descentralizada para la
creación de aplicaciones a través de la Ethereum Virtual Machine
([EVM]{.versalita}). La [EVM]{.versalita} es una «computadora» 
conformada por todas las computadoras conectadas a la red, la cual
permite la ejecución de programas para validar transacciones entre
los usuarios --parencite(ether2016)--. La naturaleza de las 
transacciones y su contenido puede variable y todas se realizan
mediante código también conocido como «contratos inteligentes». Para
compensar el costo que implica el uso de cada computadora de los 
usuarios estos son recompensados por la criptomoneda llamada ether.

El objetivo de Ethereum no es un sistema de criptomoneda, como Bitcoin,
sino la generación de una «computadora global» que una los recursos
de diversas computadoras físicas y cuyo medio de compensasión de su
uso es el pago con su propia criptomoneda. La [EVM]{.versalita} 
tendría presencia global, pero sin estar en un lugar geográfico 
determinado, haciéndola resistente a ataques e imposibilitando la
alteración de cualquier tipo de registro que comprende su cadena
de bloques. Además, al ser una «máquina» de propósito «general», es 
posible la implementación de [IA]{.versalita} que se aproxime a la 
idea de «córtex cerebral» de la humanidad.

Una implementación específica de la [EVM]{.versalita} bien puede ser
la ejecución de transacciones de información científica entre los 
pares. Ya no se trataría de libros impresos o microfilmes, sino de
documentos digitales de diversa índole que pasarían de un lector al
otro no solo de manera instantánea, sino también con la seguridad de
que el conocimiento no ha sido adulterado…

Aunque la idea del cerebro mundial como una aplicación específica de
la [EVM]{.versalita} permite un gran paralelismo entre la idea de
Wells en 1937 y lo que en la actualidad ya se perfila como posible,
la modificación de la idea hereda el optimismo tecnológico presente
en la conferencia dada por Wells.

Como bien apunta O’Dwyer, intrínsecamente las cadenas de bloques —que
sin problema también es perceptible en la [IA]{.versalita}— no van en
pos de la «cultura libre» y el acceso abierto a la información --parencite(odwyer2018)--. 
Un error en Wells es no haber pensado que el problema del acceso al 
conocimiento no solo es una cuestión de índole geográfica, sino 
también de infraestructura tecnológica que no es neutral ante las 
distintas legislaciones de la propiedad intelectual que controlan no
solo la transferencia tecnológica, sino también el acceso al 
conocimiento mediante los derechos de autor.

Las cadenas de bloques pueden ser un buen modelo utópico de un mundo
conectado y descentralizado, donde cualquiera puede tener acceso a la
información. No obstante, también puede ser el medio más perfecto de
gestión de derechos e incluso una perfecta medida tecnológica de
restricción al conocimiento mediante barreras de pagos, también 
conocido como [DRM]{.versalita} --parencite(odwyer2018)--.

La posibilidad de concreción del cerebro mundial ya no es tanto su
cuestión tecnológica, sino su pertinencia en un mundo que se debate
entre el reforzamiento de los derechos de propiedad intelectual o la
flexibilización de estas barreras económicas y jurídicas. Cuestión
que ha de atender al menos a la reflexión ética ya que el acceso a
la información no solo requiere de medidas tecnológicas que velen
por ello, sino también de una *planificación previa donde se geste
la idea de un sistema abierto*.

---

\begin{flushleft}
Autor: Ramiro Santa Ana Anguiano. \\ Escrito bajo \href{https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre}{Licencia Editorial Abierta y Libre}. \\ Contenido disponible en \href{http://xxx.cliteratu.re/}{xxx.cliteratu.re}.
\end{flushleft}
