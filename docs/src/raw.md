# Primer semestre

## 63044_t112_seminario_mercancia-capital-y-cinematografia

Asistencia:               5
Falta por mi culpa:       6
Falta por el docente:     3
Falta por otros motivos:  2

Total:                    16

## 63047_t118_curso_georges-bataille-saber-y-transgresion

Asistencia:               8
Falta por mi culpa:       4
Falta por el docente:     1
Falta por otros motivos:  1

Total:                    14

## 63047_t124_curso_filosofia-e-historia-una-lectura-politica

Asistencia:               12
Falta por mi culpa:       1
Falta por el docente:     1
Falta por otros motivos:  1

Total:                    15

---

# Segundo semestre

## 63044_t126_seminario_hegel-y-el-pensamiento-decolonial

Asistencia:               10
Falta por mi culpa:       5
Falta por el docente:     1
Falta por otros motivos:  1

Total:                    17

## 63047_t126_curso_lectura-y-comentario-de-la-fenomenologia

Asistencia:               11
Falta por mi culpa:       3
Falta por el docente:     1
Falta por otros motivos:  2

Total:                    17

## 63754_t001_seminario_tecnologia_de_la_informacion_y_sociedad

Asistencia:               6
Falta por mi culpa:       1
Falta por el docente:     6
Falta por otros motivos:  3

Total:                    16

---

# Tercer semestre

## 63044_t127_seminario_perspectivas-para-pensar-lo-vivo-desde-la-teoria-critica

Asistencia:               9
Falta por mi culpa:       4
Falta por el docente:     1
Falta por otros motivos:  3

Total:                    17

## 63057_t102_curso_analisis-de-las-sociedades-contemporaneas-desde-el-pensamiento-de-slavoj-zizek

Asistencia:               8
Falta por mi culpa:       6
Falta por el docente:     0
Falta por otros motivos:  3

Total:                    17
