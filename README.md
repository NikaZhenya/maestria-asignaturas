# Asignaturas de la Maestría en Filosofía

* Programa: Maestría en Filosofía.
* Entidad: Facultad de Filosofía y Letras de la Universidad Nacional Autónoma de México.
* Año de inscripción: 2018-1.
* Investigación: [*El creador y lo creado: la propiedad intelectual como supuesto en la creación cultural y filosófica*](https://github.com/NikaZhenya/maestria-investigacion).
* Tutor: Ernesto Priani Saisó.

Este repositorio tiene las notas tomadas durante las clases.

Ojo: no están contemplados los seminarios de investigación.

## Lista de asignaturas

### Primer semestre

| Entidad | Asignatura | Grupo | Crd |   Tipo    |          Campo          | Nombre de la asignatura | Profesor(es) |
|---------|------------|-------|-----|-----------|-------------------------|-------------------------|--------------|
|   10    |   63044    | T112  |  8  | Seminario | Filosofía de la cultura | Mercancía, capital y cinematografía | Dr. Carlos Oliva Mendoza |
|   10    |   63047    | T124  |  4  |   Curso   | Filosofía de la cultura | Georges Bataille: saber y transgresión | Dr. Ignacio González Díaz de la Serna |
|   10    |   63047    | T118  |  4  |   Curso   | Filosofía de la cultura | Filosofía e historia: una lectura política | Dr. Mario Magallón Anaya |

### Segundo semestre

| Entidad | Asignatura | Grupo | Crd |   Tipo    |          Campo          | Nombre de la asignatura | Profesor(es) |
|---------|------------|-------|-----|-----------|-------------------------|-------------------------|--------------|
|   10    |   63044    | T126  |  8  | Seminario | Filosofía de la cultura | Hegel y el pensamiento decolonial: crítica como progreso | Dr. Luis Guzmán |
|   10    |   63047    | T126  |  4  |   Curso   | Filosofía de la cultura | Lectura y comentario de La fenomenología del espíritu de Hegel | Dra. Virginia López-Domínguez |
|   10    |   63754    | T001  |  4  | Seminario | Maestría en Bibliotecología y Estudios de la Información | Tecnología de la información y sociedad | Dra. Georgina Araceli Torres Vargas |

### Tercer semestre

| Entidad | Asignatura | Grupo | Crd |   Tipo    |          Campo          | Nombre de la asignatura | Profesor(es) |
|---------|------------|-------|-----|-----------|-------------------------|-------------------------|--------------|
|   10    |   63057    | T102  |  4  |   Curso   | Filosofía de la cultura | Análisis de las sociedades contemporáneas desde el pensamiento de Slavoj Zizek | Dra. Mariflor Aguilar Rivero y Dra. Laura Echavarría Canto |
|   10    |   63044    | T127  |  4  | Seminario | Filosofía de la cultura | Perspectivas para pensar lo vivo desde la teoría crítica: Arendt, Foucault y Butler | Dra. María Antonia González Valerio y Dra. Rosaura Martínez Ruiz |

### Cuarto semestre

No más clases.

## Licencia

Los apuntes están bajo [Licencia Editorial Abierta y Libre (LEAL)](https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre).



