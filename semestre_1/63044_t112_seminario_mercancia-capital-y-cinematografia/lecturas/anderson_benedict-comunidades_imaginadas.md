# *Comunidades imaginadas*, Benedict Anderson

## I. Introducción

El nacionalismo como anomalía incómoda para la teoría marxista.
=> La «calidad de nación» es un artefacto cultural de una clase en particular.

Definición operativa de nación: una comunidad política imaginada como
inherentemente limitada y soberana.
  - Imaginada porque los miembros nunca se conocerán unos a otros pero en cada
    uno vive la imagen de su comunión.
=> ¿Qué hace que las imágenes contrahechas generen sacrificios?
  - Quizá ligado a las raíces culturales del nacionalismo.

## II. Las raíces culturales

Siglo XVIII en Europa: surgimiento del nacionalismo y crepúsculo del 
pensamiento religioso.
  - El nacionalismo no se alinea con ideologías políticas.
  - El nacionalismo se alinea con grandes sistemas culturales.
    1. Comunidad religiosa.
    2. Reino dinástico.
=> Se daban por sentadas, como ocurre ahora con la nacionalidad.
  - ¿Por qué?
  - ¿Por qué decayeron?

Las grandes comunidades clásicas:
  - Se concebían como centrales.
  - Usaban una lengua sagrada.
=> Supone que el signo no es arbitrario.
  - Lenguas «verdaderas».
  - Letrados como conexión entre la tierra y el cielo que legitiman a la
    comunidad religiosa.

Su coherencia inconsciente se desvaneció por:
  1. Las exploraciones como apertura de horizontes que cambiaron
     la concepción del hombre.
  2. Degradación de la lengua sagrada.

Los reinos dinásticos se expandieron por:
  - La guerra.
  - La política sexual.
=> A partir del siglo XVII se pierde legitimidad.
  - La nacionalidad como una búsqueda de legitimación.

Las naciones no solo surgen de la declinación de las comunidades religiosas
o reinos dinásticos.
  - También hubo un modo de aprehensión del mundo que permitió pensar a
    la nación.

Transformación de la percepción del tiempo, p. ej.:
  - La novela.
  - El periódico.
=> Colación de la ficción en la realidad de modo silencioso.

La posibilidad de imaginar la nación surgió por:
  1. La lengua escrita ya no como un privilegio.
  2. Pérdida de una sociedad organizada bajo centros elevados.
  3. Temporalidad que ya distingue entre la cosmología y la historia.
=> Necesidad de algo que vuelva a dar sentido.

## III. El origen de la conciencia nacional

La imprenta como mercancía permiten la generación de ideas de simultaneidad.
  - Posibilidad de comunidades «horizontal-secular».

El capitalismo dio impulso a las lenguas vernáculas cuando el mercado
de libros en latín se saturó.
  - Forzado por tres factores externos:
    1. El latín se alejó de la vida eclesiástica.
    2. El éxito de la Reforma por su impresión en lenguas vernáculas.
    3. Difusión lenta de las lenguas vernáculas como centralización
       administrativa.

Las lenguas vernáculas como lenguas de poder ayudó a la decadencia de
las comunidades religiosas.
  - Interacción entre la fatalidad, la tecnología y el capitalismo.
    - El capitalismo permitió el desarrollo de las lenguas vernáculas
      mediante la reproducción mecánica.

Bases de cómo las lenguas impresas sentaron la conciencia nacional:
  1. Creación de un campo unificado de comunicación, debajo del latín y
     las lenguas vernáculas habladas.
  2. Fijeza del lenguaje por parte del capitalismo impreso.
  3. Creación de lenguas de poder a partir de lenguas vernáculas 
     administrativas.

La discontinuidad entre las lenguas impresas, las conciencias nacionales y
los Estado nacionales se enmenda con la creación de nuevas entidades políticas.

## IV. Los pioneros criollos

Los nuevos Estados americanos implican dos factores:
  1. La lengua no fue un punto de controversia; era común entre las colonias
     y las metrópolis.
  2. No se trató de llevar a las clases bajas a la vida política.
=> Concepción temprana de la nacionalidad según el estándar europeo.
  - Dos factores comúnmente aducidos:
    1. Fortalecimiento del control de Madrid.
    2. Difusión de las ideas ilustradas.
  => Explica de forma general, no en modo particular.
    - No explica el sacrificio de las clases acomodadas.
      - Una explicación insuficiente es que fue debido a cuestiones de mercado
        o político administrativas.

Explicación de cómo las unidades administrativas pasaron a concebirse como
patrias:
  1. La unificación significa intercambio interno de hombres y documentos.
  2. Expansión extraeuropea para el desarrollo de burocracias 
     transcontinentales.
    - No ocurrió, era una operación irregular, los criollos no tenían posición
      de importancia.
      - Los criollos por naturaleza los consideraban distintos e inferiores a
        los peninsulares: fomento a la aparición de la conciencia americana.
  => Criollos peregrinos.
  3. Llegada del capitalismo impreso, principalmente el periódico.
    - Reunían la misma estructura administrativa y de mercado.
    - Era local y plural (conciencia de otros medios impresos semejantes en
      Hispanoamérica).
    - Mundo imaginado de lectores.
  => Impresores criollos.
=> Desarrollo de la resistencia en clave nacional y plural.

## V. Lenguas antiguas, modelos nuevos

Dos características que diferencian el nacionalismo europeo del americano.
  1. La lengua jamás fue un tema de controversia en América.
  2. En América se pudo funcionar con base en modelos precedentes.
=> En América la «nación» fue algo concientemente deseado desde un principio.
  - Invento que no podía patentarse, solo piratearse.
  => Análisis de la lengua impresa y la piratería.

La lengua pasó a un campo interno, creado y consumado por sus mismos hablantes.
  - Latín, griego y hebreo pasan a ser ontológicamente iguales a las lenguas
    vernáculas.
  - Antes del capitalismo impreso.
=> Revolución lexicográfica.

El ascenso de la burocracia está relacionado con el capitalismo impreso.
  - A mayor alfabetización, mayor apoto popular.
=> Creación de un modelo pirateable de Estado nacional.
  - Por su popularidad tenía ciertas normas sobre las desviaciones no 
    permitidas.

## VI. El nacionalismo oficial y el imperialismo

La revolución lexicográfica surgió de necesidades administrativas, pero
gradualmente crece la convicción de ser una propiedad de grupos específicos.
  - Surgimiento de los «nacionalismos oficiales»: estirar la piel de la nación
    al cuerpo del imperio.
    - Fusión voluntaria entre nación e imperio dinástico.
    - Reacción a los movimientos nacionales populares.
    - Una mancuerna fue la imposición de lenguas vernáculas en la educación.
  => Existe una incompatibilidad entre el imperio y la nación.
    - Problema del racismo.
    - Excluye comunidades jóvenes.
    - Fiesta ilusoria: en el núcleo del imperio también estaban surgiendo
      naciones.
  => Imposibilidad con la aparición de los nacionalismos lingüísticos populares.

