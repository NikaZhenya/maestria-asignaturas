# 8 de agosto

El seminario es sobre estudios sobre formación de identidades
=> Hipótesis: las identidades se desarrollan a través del capitas y las mercancías.
  - *El capital* como punto de partida.

La «mercancía nómada» ya no está enraizada en la tierra.
  - Son virtuales.
  - La mercancía punta es la comunicación.
  - Mariflor Aguilar trabaja el concepto en *Resistir es construir*.

Tres grandes mercancías según Echeverría:
  - Nación.
  - Democracia.
  - Revolución.
    * Concentra la posibilidad de cambio tecnológico de toda mercancía.
    * Perfección del proceso mercantil.

Anderson: un gran impulso cuando el capital de vuelve capital impreso.
  - Necesidad de analizar la América española.

Kraniauskas: la verdadera historia del capital está en América Latina.

La mercancía es espectral. [¿Hirose y Zizek?]
  - Nunca se conoce totalmente.

Hipotesis fuerte de Oliva: el capital no produce ideología.

# 22 de agosto

Análisis del primer párrafo de *El capital*.
  - Dos hipótesis de Oliva sobre el estudio del capital:
    1. Fenomenológico.
    2. Monadológico.
=> No hay sustancia en el capital y, por ende, de «socialidad primaria»
  - Através de la mercancía.

Se dice que Marx no debió partir el estudio del capital con el análisis de la
mercancía.
  - Según esta crítica, Marx no ve la relevancia en:
    - La fuerza de trabajo.
    - La tecnología.
=> Oliva lo considera incorrecto porque la mercancía requiere abstracción.
  - Una mónada que se comunica mediante el dinero.

Mercancía es:
  - Valor de cambio (valor sin más).
    - Generar equivalencia mediante la abstracción.
      1. Abstracción del trabajo social.
      2. Abstracción del dinero.
  - Valor de uso.
    - Depende de una disciplina: el consumo.
    - Sostiene el valor de cambio.

El capital como ensayo de destrucción que nunca se destruye.
  - Es espectral, lo que permite que resista y absorba su crítica.

# 29 de agosto

No hubo clase.

# 5 de septiembre

No asistí

# 12 de septiembre

En el cine, la cámara define cómo ver al objeto.
  - Visualización del corto «Pedro y el lobo».

En el cine americano la construcción de la subjetividad implica la creación
de una moral.
  - Hay buenos y malos.
  - Cine de acción.

Apertura de la conciencia:
  - Acción => Cine americano.
  - Conciencia => Cine europeo.
  - Paisaje => Cine asiático y latinoamericano.

Cuatro tesis de Anderson:
  1. El nacionalismo es anómalo en la teoría marxista.
  2. El nacionalismo es un artefacto cultural de una clase.
  3. Hay un movimiento discreto en el nacionalismo.
  4. La nación es una comunidad imaginada.

# 19 de septiembre

No hubo clases por temblor.

# 26 de septiembre

No hubo clases por toma de las instalaciones.

# 3 de octubre

La subsunción de la modernidad al capitalismo es mediante el capitalismo 
impreso:
  - Calendario.
  - Reloj.
  - Mapa.

El sistema mercantil forma a sus actuantes.

<!-- Leer primer capítulo de *Cine capital* -->

# 10 de octubre

No asistí.

# 17 de octubre

No asistí.

# 24 de octubre

No hubo clase.

# 31 de octubre

No asistí.

# 7 de noviembre

No asistí.

# 14 de noviembre

Análisis de la película *Nostalgia*.
  - Reproducción del patriarcado en el cine.

Hume: la genealidad viene del trato con los materiales.

La mercancía principal en la actualidad es la comunicación (!?).

<!-- La PI regula varios espectros como el tecnológico, la práctica científica,
     el discurso, el «origen» y la «persona» -->

# 21 de noviembre

No hubo clases.

# 28 de noviembre

No asistí.
