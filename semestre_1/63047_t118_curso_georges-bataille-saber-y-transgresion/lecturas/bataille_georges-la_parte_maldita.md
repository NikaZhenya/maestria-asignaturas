# *La parte maldita*, Georges Bataille

## Primera parte: introducción teórica

### El sentido de la economía general

La economía se estudia como un sistema aislable e independiente.
  - Método legítimo y científico.
  - No da los mismos resultados a la física porque en la economía:
    - Los fenómenos no son fáciles de aislar.
    - No es fácil establecer una coordinación general.
=> Economía como sistema deproducción y consumo humanos.
  - ¿Qué tal si se estudia en un conjunto más vasto?
  - ¿No se deben plantear unidos al movimiento de la energía en el globo?

Más allá delos fines inmediatos, las acciones son inútiles e infinitas en un
plano universal.
  - ¿La actividad humana altera esto mediante la apropiación de esta energía?

Un organismo en principio tiene más energía de la necesaria para 
su mantenimiento.
=> Energía excedente = riqueza
  - Se utiliza para crecer.
  - Sino se puede crecer más, se tiene que perder voluntariamente.
    - De modo gloriosoo catastrófico.

La necesidad de despilfarro va en contra de la economía razonable.
  - De manera general solo es posible emplear la energía en la medida de
    las capacidades de la humanidad económica.
  => Humanidad económica vista como organismo.

El hombre reduce las operaciones económicas en sistemas particulares y
articulablesentre sí para un fin limitado.
  - La ciencia económica generaliza sin tener en cuenta a la energía ilimitada.
    => Energía ilimitada = materia viviente en general.
  - Aunque se intente, la energía se escapa y se pierde.
=> Los excesos de fuerza vivaesel factor de ruina más peligroso.
  - Intentos de descongestión:
    - Fiestas.
    - Edificación de manumentos inútiles.
    - Multiplicación de servicios para facilitarla vida.
  => Insuficientes: lleva a la destrucción de las guerras.

La actividad guerrera pudo ser frenada cuando el excedente se usó para el
desarrollo de la industria.
  - Explica la paz relativa en la revolución industrial.
  - Generó un excedente mayor.
=> Muchos niegan esta hipótesispero las guerras mundiales destruyeron
   el excedente.
  - + exceso = + intensidad en la destrucción.

La guerra amenazante puede evitarse mediante:
  1. La extensión racional de un crecimiento industrial costoso.
  2. La constitución de obras improductivas, disipadoras de energía.
=> Desatan problemas más complejos.

El cambio de la economía restringida a la general es un giro copernicano.
  - El crecimiento exige una pérdida de una parte de las mercancías.

### Leyes de la economía general

Para el crecimiento y la reproducción es necesaria una energía excedente.
  - Todo organismo da tributo de energía.
  - El fin del crecimiento llega en la madurez sexual.
  - La reproducción es el paso del crecimiento individual al grupal.

La fuente de la riqueza está en la radiación solar.
  - Génesis de dos juicios morales:
    1. Gloria improductiva (antigüedad) que enfatiza el gasto de la energía.
    2. Gloria productiva (cristiandad) que enfatiza la adquisión de la energía.
  => Una vez que se crece solo queda dilapidar o irradiar.
    - La limitación puede darse entre individuos y grupos, pero la única
      limitación real es la biosfera, el crecimiento global.

Del crecimiento viene la presión: la ocupación de un espacio se da de
la misma forma a su espacio vecino.
  - Al menos que sea un espacio desfavorecido.
  - Implica un gran esfuerzo para evitarlo.
=> Difícilmente entra en nuestros cálculos porque se considerar intereses en
   lugar de deseos.
  - La acción racional implica una consideración utilitaria:
    ganancia, mantenimiento o crecimiento.
  - La exuberancia supone un límite racional, es pérdida, agradable o no;
    es decir, sentimiento.

La definición de la presión es inexacto pero es posible describir sus efectos.
  - Las posibilidades de vida están limitadas por el espacio.

Primer efecto de la presión: la extensión.
  - Se pretende aumentar sus límites.

Segundo efecto de la presión: la dilapidación o el lujo.
  - Refriega para ajustar sus límites.
    - Muerte como forma más importante.
  - No hay crecimiento pero permite desarrollo de vida más exuberantes.

La depredación es la forma más simple de lujo.
  - Destrucción para hacer espacio.
  - Estar en la cima precisa una inmensa lapidación de la energía.
  - La muerte se comporta como accidental.

La muerte en forma inexorable es el lujo más costoso.
  - La muerte reparte en el tiempo el espacio necesario para nuevas
    generaciones.
    - Se maldice algo necesario para la existencia.
=> Miedo a nosotros mismos.
  - Nos mentimos para creer escapar de esta dinámica.
  - Termina sintiéndose con mayor intensidad.
  => Negación de nosotros mismos para desatar en verdad profunda.

La reproducción es el mayor lujo.
  - Acompaña a todas las formas de ruina: hecatombe hasta la muerte.

La actividad humana está condicionada por el movimiento natural de la vida.
  - La técnica es la posibilidad de ampliar el crecimiento en sus límites
    de lo posible.
    - Ni continuo ni infinito.
    - En modo de impulsos.
    - Base para un crecimiento biológico.
    - Generación de unmayor excedente que necesita gastarse.
=> Por su complejidades difícil de determinar en tipos concretos.

El hombre es el más apto para consumir lujosamente.
  - Beneficio de servicios improductivos gracias a una técnica que reduce
    el trabajo y aumenta su salario.
=> Parte maldita: la maldición de rechazar la dilapidación lujosa cuando
   es necesaria para la reducción a sus límites más justos.
  - Una paradoja donde la libertad:
    - No es una voluntad de asumir riesgos.
    - Es una garantía contra la servidumbre.

La maldición supone la angustia que significa la ausencia de presión.
  - La angustia acontece cuando no se está orientado por la superabundancia.
    - Implica que es personal.
    - Implica que no existe para quien desborda de vida o para el conjunto
      de vida.

En la existencia particular siempre se está en peligro de sucumbir, pero en
la existencia general los recursos se encuentran en exceso por lo que la
muerte no tiene sentido.
  - En lo particular el problema es la insuficiencia de recursos.
  - En lo general es el exceso de recursos.
=> Implica en un plano general impulsar el crecimiento considerando los
   límites y reducir la presión [¿distribución de la riqueza?].
  - P. ej., aliviar el excedente americano no como guerra, sino elevando el
    nivel de vida global.

La aplicación es difícil.
  - En la economía general tiene un carácter explosivo.
  - La maldición es no tener la fuerza de encausar este movimiento vertiginoso.
=> Su superación depende del hombre y su conciencia de sí.
  - La huida de la verdad es su reconocimiento.
  - La conciencia se alcanza en una visión lúcida del encadenamiento histórico.
  => Necesidad de relacionar datos históricos para dar sentido a
     los datos presentes.

## Segunda parte: datos históricos I

### Sacrificio y guerras de los aztecas

#### 1

Un principio: el movimiento cuyo efecto es la prodigalidad está lejos de ser
siempre el mismo.
  - El exceso puede ser pérdida o crecimiento.
    - El crecimiento es transitorio, no dura infinitamente.
  => El hombre es distinto en obras en crecimiento o en sociedades estables.
    - En la estabilización se civiliza y se confunde esta suavilización con
      el valor de la vida.

#### 2

El mundo de los aztecas es diametralmente opuesto desde la perspectiva de la
actividad.
  - La consumación por medio del sacrificio.
  - El pensamiento como exposición de los actos.

#### 3

El sacrificio como la cima del rito religioso.
  - La mayoría eran prisioneros de guerra.
    - Las guerras eran consumación, no conquistas.

#### 4

Los prisioneros eran tratados humanamente.
  - Se intentaba mitigar su angustia con baile, canto, emborrachamiento y
    prostitución.

#### 5

Encadenamiento entre la guerra y el sacrificio.
  - El vencedor se quedaba con la cabeza después del sacrificio.
  - Los guerreros venían a morir en combate, sea en el combate mismo o en
    el sacrificio.

#### 6

La sociedad mexicana no era militar, sino religiosa.
  - No conocieron la organización racional de la guerra y de la conquista.
    - Nada más contrario que la dilapidación de la riqueza por medio del
      sacrificio.
  - El sacrificio era violencia interior y exterior.

#### 7

El sacrificio se muestra como necesidad lógica.
  - La religión como gran esfuerzo y búsqueda angustiosa.

De lo real a lo divino por medio de la destrucción sin beneficio del
encadenamiento de las obras útiles.
  - El sacrificio como consagración destructora.
    - La víctima es un excedente útil que ha de aniquilarse: la parte
      maldita que será liberada del orden de las cosas.
  => Disminución de la pesadez que introducía la avaricia y el cálculo
     del orden real.

### El don de rivalidad

#### 1

El sacrificio hacia improductivo una gran parte de los recursos disponibles.
  - Las fiestas eran derramamiento de sangra y de riquezas.
    - Patrocinadas por los ricos, entre ellos los mercaderes.

#### 2

El mercader azteca no vendía, sino que practicaba el intercambio de dones.
  - El objeto no era cosa condenadaa la inercia y a la ausencia de vida en
    el mundo profano.
  - El objeto era don, el esplendor de la gloria.
=>Solo entendible con el potlatch.

#### 3

En la economía clásica se imaginan los primeros intercambios como trueque.
=> vs: no era necesidad de adquirir, sino de derrochar.
  - Costumbre gloriosa.
=> En nuestros días es perceptible en el potlatch:
  - Medio de circulación de riqueza que excluye el regateo.
  - Donación solemne de riquezas considerables, pensado para humillar o
    desafiar.
    - Se responde al desafío con un nuevo potlatch más generoso.

#### 5 [4]

La pérdida del donador es solo aparente.
  - Tiene poder sobre el donatario.
    - El donatario se ve obligado a destruirlo con la devolución del don,
      con usura.
  - Donar es perder pero también compensa a quien lo hace.
  - No responde a una necesidad de ganar, sino de la continuidad del potlatch.

#### 6 [5]

El potlatch incrementa los dones por medio de la revancha que confiere un don.
  - No equivale a poder, o si lo es, no es como fuerza o derecho, sino
    como poder de perder.
  - La gloria como el gasto de energía sin medida para la adquisición de
    un rango.

#### 7 [6]

El potlatch *no es* igual a:
  - Rapiña.
  - Intercambio lucrativo.
  - Apropiación de bienes.
=> Es ambigüedad fundamental que tiene ciertas leyes:
  - Cuando el objeto no puede ser una apropiación,
    su dilapidación se convierte en el modo de apropiarlo.
  - El prestigio es la apropiación en la lapidación.
  - También el rango es apropiado.

#### 8 [7]

Cuando hay exceso y el crecimiento ya no es posible, el derroche es necesario.
  - El derroche puede seruna forma de ganancia.
    - Ganancia de prestigio y muestra de superioridad.
  => Contradicción: el valor, el prestigio y la verdad de la vida se colocan
     en la negación del empleo servil de los bienes, al mismo tiempo que
     su negación es el empleo servil.

El rango es lo opuesto a una cosa.
  - Fundamento sagrado.
  - Ordenación jerárquica.
=> No elimina su sentido.
  - Reduce a cosa.

#### 9 [8]

La acumulación individual de recursos está destinada a la destrucción.
  - El sacrificio y el potlatch evitan el consumo productivo.
    - Uno aniquila, el otro dona.

El potlatch es la forma más significativa de lujo.
  - El lujo auténtico exige un completo desprecio de las riquezas:
    un insulto callado a la mentira laboriosa de los ricos.
    - La riqueza actual es falsificación y miseria.
    - El lujo se encuentra en el miserable.

## Tercera parte: datos históricos II

### La sociedad conquistadora: el Islam

*Islam* significa sumisión, sometimiento a Dios.
  - Opuesta al individualismo.

La *Hadith*, la tradición escrita, organizó sistemáticamente a la conquista.
  - Excluye violencia inútil.
  - Pacto humano con los vencedores.
  - Respetoa las obras de irrigación.

La *Jihad* es la guerra santa permanente en las fronteras del islam.
  - Noción que se opone a los hechos, por lo que surge un expediente jurídico,
    el *hila*, que permite la creación de treguas y su anulamiento.
=> Método de crecimiento indefinido.

Antes de la hégira los árabes se ordenaban en tribús donde el don y el derroche
eran frecuentes, así como el sacrificio.
  - Necesidad de principios y unificación de fuerzas.

Mahoma opone el *don*, la fe, con el *muruwa*, el ideal de virilidad individual
preislámica.
  - Prohibición de la venganza con sangre entre musulmanes.
  - Condena de infanticidio,alcoholismo y rivalidad.
  - Sustitución por lo socialmente útil.
=> Esta austeridad sin disipación permite el crecimiento de fuerzas.
  - No puede ser indefinido.
  - La hégira fue el inicio y dadora de sentido.
    - El fundamento es la comunidad, no la sangre ni el territorio.
    - Jefe religio tamibén jefe político.

Los recursos desperdiciados pasaron al servicio de la comunidad armada.
  - La conquista como sistema.
=> Pero la búsqueda de un poder mayor hace perser a la vida del poder de
   disposición.

El islam no es consumo, sino como el capitalismo, acumulación de fuerzas
disponibles.
  - Limitó lo religiosoa la moral, la limosna y la oración: pérdidade sentido.
    - En las tribus opuestas al islam subsistió la actitud guerrera.

### La sociedad desarmada: el lamaismo

El Tibet como país sin fuerza militar.
  - Análisis del Tibet moderno mediante los escritos de un inglés.

El budismo fue introducido en 640.
  - Gobernado por reyes.
  - Con poder militar sobre la región.
  - Reforma budista severa en el siglo XI.
  - Con apoyo mongol se derroca al rey y da paso a los Dalai-Lama.

Dalái lama es un título mongol y no representaba al más importante de los dioses
encarnados.
  - Dominio frágil: sin fanatismo religioso u obediencia militar.
=> Vasallaje a China a partir del siglo XVII.

Entre sucesiones hay un periodo de veinte años de regencia mientras madura el
dalái lama.
  - Un sujeto más dócil para los intereses chinos.

La influencia inglesa llega durante el treceavo dalái lama.
  - Búsqueda de apertura del mercado tibetano para India.
  - Temor de influencia rusa.
  - Intervención militar que provocó:
    - Huída del dalái lama a China.
    - Apertura de tres ciudades al comercio.
    - Reconocimiento del protectorado a una provincia.
    - Imposibilidad de intervención de otras naciones.

El treceavo dalái lama fue perseguido por los chinos.
  - Los ingleses no lo acogieron.
=> La caída del imperio chino permitió retomar el poder.
  - Adquisición de la experiencia del poder.
    - Solo había adquiridolos conocimientos de un monje.
    - Tomó conciencia del juego de fuerzas exteriores.
    - Reconocimiento de los límites de su fuerza religiosa.

Dalái lama ofrece a los ingleses su soberanía exterior, con la condición de no
entrometerse con la política interior.
  - Inglaterra aceptó.
  - Dalái lama cae en cuenta que el poder no puede ser ejercido sin estar
    abierto al exterior y que en el exterior solo se puede esperar la muerte.

El treceavo dalái lama decidió empezar a organizarse militarmente.
  - Impuesto a todas las propiedades para su manutención.
=> Quita influencia a los monjes.
  - Provoca una rebelión de los monjes.
    - La política militar era de prudencia.
    - La rebelión amenazó el propio monaquismo.
  => Fracaso.

El presupuesto de la iglesia tibetana era:
  - Doble al del Estado.
  - Ocho veces más al del ejército.
=> La fuerza monástica impide la fuerza militar.
  - Aunque la creación del ejército es una decisión racional, en este caso es
    contrario al fundamento de la vida.
  - Su integración al mundo significa su desaparición.

El expediente cambia estructuras, donde el crecimiento tiene numerosas formas.
  - Demográfico.
  - Militar.
  - Religioso.
  - Ocio.
  - Lujo.
=> Cuando una llega a su límite, viene otra forma que refunda sus leyes morales.
  - El Tibet inventó el monaquismo como modo de gasto del excedente.

El excedente del Tibet fue puesto en monasterios, que al mismo tiempo provocó la
disminución de las riquezas.
  - Paradoja del lamaísmo.
  - Pobreza tal que no interesa a las naciones vecinas.
  - El monaquismo como detención del crecimiento de un sistema cerrado.
    - Gasto puro y renuncia al gasto al unísono.

## Cuarta parte: datos históricos III

### Los orígenes del capitalismo y la reforma

Existe cierta relación entre los protestantes y la reforma capitalista:
  - No Lutero, sino Calvino, por haber expresado los intereses de la clase 
    media.

Mundos religiosos diferentes = tipos de economía opuestos.
  - Catolicismo romano y economía precapitalista.
  - Protestantismo y economía moderna.
=> La diferencia es que en el primero el exceso se traduce en improductividad
   mientras que en el otro determina el crecimiento.

En la Edad Media la actividad productiva quedaba subordinada a las leyes de la
moral cristiana.
  - Jerarquía de funciones.
    - Clero.
    - Aristocracia militar.
    - Productores.
      - Dan su trabajo a cambio de protección y participación en la vida divina.
      - No hay autonomía de las actividades económicas.

El préstamo con interés implicaba los factores de dinero más tiempo.
  - Pero el tiempo es cosa de Dios.
=> Oposición al libre desarrollo de las fuerzas productivas.
  - Concepción racional, moral y estática del orden económico.

El desarrollo se da por la apertura de territorios, las transformaciones 
técnicas y la aparición de productos nuevos.

La religión, las actividades religiosas y la construcción de obras pías es la
manera en como se usa el excedente.
  - Destrucción de su valor útil.
=> Lutero: vs los méritos adquiridos por estas medidas.
  - No atacó el lujo, sino este en relación con el evangelio.
  - Separación de Dios de la actividad humana.
    - Riqueza como valor productivo.
=> Negación de la consumación intensa de recursos.

Lutero mantenía la visión arcaica de la economía.
  - Calvino no, reconoció la moralidad del comercio.
    - El calvinismo como la religión de la burguesía.

Calvino es a la burguesía lo que Marx es al proletariado.
  - Aportó organización y doctrina.

Calvino rechaza el mérito como Lutero, pero:
  - La finalidad es la glorificación de Dios pero mediante la acción: el 
    trabajo.
  - Negación del gasto inútil.
  - Modestia, ahorro y trabajo.
  - Renuncia a todo halo de esplendor.
=> Separación de la gloria divina.
  - Afirmación del valor de las empresas.

El capitalismo no solo es acumulación de riquezas por medio de empresas 
comerciales.
  - También implica un individualismo que no podía coexistir con las viejas
    legislaciones.
    - Hasta el siglo XVII se da la independencia de las leyes económicas.
    - No se anuncia directamente para evitar conflictos con la Iglesia.

Un sentido profundo de la Reforma es el haber abierto pasoauna nueva economía.
  - La radicalidad de la pureza religiosa ocasionó la destrucción del mundo
    sagrado y de la consumación improductiva.
    - Aparición de la burguesía y la realización humana como economía.

### El mundo burgués

<!-- Falta p. 161 -->

El mundo moderno no está alejado de Calvino pero actúa de diferente forma.
  - No busca nada ilusorio.
  - La conquista es resolver los problemas creados por las cosas.
=> Reencontrar la verdad implica resolver el problema de la economía.
  - Libertad como respuesta a las exigencias propias de las cosas.
    - No conseguía más que cosas y tomará su sombra.

El calvinismo, cuya consecuencia es el capitalismo, plantea: ¿cómo encontrarse
si la búsqueda lo aleja de sí mismo?

La cosa es exterioridad, es lo dado como realidad física.
  - No puede penetrarse.
  - Su sentido es el de cualidades material y productiva.
  - Con Calvino la cosa comenzó a dominar al hombre.
    - Se empezó a vivir en la empresa y cada vez menos en el tiempo presente.

El carácter radical de Calvino deviene en una multitud abandonada a la 
producción, a la existencia mecánica de la cosa.
  - El marxismo toma esta rigurosidad y excluye la búsqueda directa de sí mismo.

Marx plantea claramente lo que el calvinismo había esbozado: una independencia
de la cosa respecto de otras inquietudes.
  - La originalidad de Marx consiste en alcanzar un resultado moral de modo
    negativo.
    - Mediante la supresión de los obstáculos.
    - Pretensión de reducir las cosas al hombre.
    - Crítica al capitalismo por haber liberado las cosas sin rigor.

La burguesía no fue conciente de la oposición que se halla en el crecimiento;
es decir, el lujo.
  - Creación del mundo de la confusión.
    - Reducción del hombre a cosa.

Complicidad de la burguesía = conciencia oscura de no ser más que cosas.
  - De aquí surge el retorno del hombre a sí mismo.
    - Dedicación del excedente al allanamiento de dificultades de la vida.
    - Rigor que es encadenamiento racional de las cosas.
=> Un sentido revolucionario formulado por Marx.

La supervivencia del feudalismo, que el capitalismo olvida, es hacer cosa al
obrero.
  - El obrero es cosa cuando la liberación es la entrega de una obra.
=> El regreso del hombre exige desenmascarar los rostros engañosos de la
   aristocracia.
  - Posición radical con consecuencias políticas.
  - Liberación del hombre a partir de la cosa.
  - Atención a lo que está ahí.
=> Consecuencia extraña:da al burgués el sentimiento de mantener la libertad de
   los hombre cuando el militante obrero es subordinado.

## Quinta parte: datos actuales

### La industrialización soviética

La URSS como el único país seguro de sí mismo y con una férrea voluntad de
organización.
  - El odio hacia la URSS se basa en su completa negación de la autónoma
    realidad individual.

La URSS como un imperialismo: un Estado universal.
  - Estado en un lugar preponderante, como en Hegel.
    - El hombre no como individuo, sino como Estado.
  - Trata el problema de la distribución de la riqueza.
    - Las reivindicaciones obreras aumentan el costo de producción, por lo que
      disminuye el lujo de los patrones y la acumulación.
  => Mayor gasto improductivo.
    - Solo da al hombre mayor disposición de sí mismo.

Rusia estaba gobernada por una clase incapaz de acumular.
  - Inagotables recursos en territorios inexplorados.
  - Industrialización hasta el siglo XIX.
    - Dependencia del capital extranjero.
  - Lucha revolucionaria como disminución del gasto improductivo y aumento en
    la acumulación para el equipamiento industrial.
    - Paradoja del proletariado: autoimposición de renuncia a la vida para
      hacerla posible.
=> Creación de uno de los sistemas que más conserva el excedente para la
   creación de fuerzas de producción.

La única posibilidad es el trabajo y la construcción de una industria para el
porvenir.
  - Implica migración del campo a la ciudad que, para evitar el desequilibrio,
    se «colectiviza» la tierra y se equipa con máquinas.
=> El atraso era tal que la vía violenta fue la única salida.
  - Actualmente es tensión porque no retrocede ante los problemas reales de
    la Revolución.

### El plan Marshall

El plan Marshal como el proyecto sistemático de oposición a la URSS.
  - La lucha es de dos modelos económicos.
  - Economía a escala internacional.
  - Donación del producto del trabajo.
    - Renuncia al principio del beneficio para evitar sus consecuencias.
  - Inversión de interés mundial.
    - El análisis clásico no sirve de ayuda.
=> Movilización del capital.
  - Oposición a las operaciones aisladas del tipo clásico.
  - Operación general de renuncia al crecimiento.
    - Riqueza condenada.
  - Profunda negación del capitalismo.

El plan Marshall es consecuencia de una agitación obrera.
  - La URSS tiene la llave.
  - Plantea la necesidad de injerencia americana.
  - Ayuda a la evolución pacífica de las instituciones.
  - Eleva el nivel de vida.
=> Evolución pacífica donde distribucción = realización del capitalismo.

## Epílogo

### La teoría del excedente medio siglo después

La teoría del excedente tiene un potencial para convertirse en un nuevo
paradigma científico: el paradigma de la abundancia.
  - La economía tradicional descansa sobre el paradigma de la escasez.

El abordaje común de Bataille es mediante sus experiencias vitales.
  - Su importancia reside en su carácter sugestivo.

¿Qué lugar puede ocupar esta obra en la economía?
  - Primeros economistas.
    - Adam Smith y David Ricardo.
    - Se concentran a largo plazo y la explicación del crecimiento de la 
      riqueza.
    - Recomendación del libre mercado.
  => Solo apto para los primeros países industrializados.
    - Perspectiva nacional y macroeconómica.
  - A partir de 1850.
    - Hay riqueza acumulada muy considerable.
    - El énfasis pasa de la producción al consumo.
  => Perspectiva individualista, finalista y microeconómica.
  - Primeras décadas del siglo XX.
    - Alfred Marshall.
    - Enfoque al productor y el beneficio.
  => Momento de esplendor después de la WWI.
  - 1929.
    - Gran Depresión.
    - Pesimismo económico.
    - Crisis en la ciencia económica.
    - Keynes y su crítica a Marshall.
  - 1936.
    - Keynes publica su teoría.
    - Revolución económica.
    - Inyectar dinero para provocar la demanda.
    - Acento en el consumo.
  => Tiempo en que Bataille desarrolla su teoría.

La crítica económica de Bataille:
  - Se centra en el concepto de utilidad y en el particularismo económico.
    - Ambas nociones como reflejo de valores culturales.
  - En rasgos generales semejante a Keynes.
    - Bataille no formalizó.
    - Teoría del excedente ≠ teoríadel empleo.
    - Keynes pone acento en el consumo para promover la producción.
    - Bataille tiene una noción más amplica del consumo: gasto improductivo,
      dilapidación, derroche y lujo.
    - Bataille no defiende al capitalismo, su noción es pérdida por gusto y
      transformación revolucionaria.
  - Teoría original que explica el porqué del desarrollo económico.
    - Con planteamientos físico-biológicos.
    - Manifestación del excedente de la Tierra.
    - La limitación se da en el espacio.
    - Articulación antropológica y económica.
  - La guerra como consumación del excedente.
    - Facilita el desarrollo de instituciones alternativas.
  - Teoría de ciclos económicos y de culturas como organismos vivientes.
    - Evolucionista convencido -> cuestión ya superada.
  - Teoría a escala planetaria.
