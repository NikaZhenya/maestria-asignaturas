# *Prefacio a la transgresión*, Michel Foucault

La sexualidad nunca ha tenido un sentido más natural sino hasta el mundo
cristiano.
  - El misticismo lo comprueba al no separar las formas continuas del deseo.
=> La sexualidad moderna ha sido desnaturalizada.
  - Puesta al límite, es hendidura.
  - Profranación a sí misma.
=> Es transgresión: recomponer lo sagrado en su forma vacía.
  - El lenguaje y los gestos como la única manera de dar con lo sagrado =>
    muerte de Dios.
    - Experiencia no exterior al ser: interior y soberana.
    - Experiencia de lo imposible: el reino ilimitado del límite.
  - Bataille como ceniza prometedora.

La transgresión concierne al límite.
  - Franquea y sitúa en la incertidumbre.
  - Puesta al límite, encuentro en lo que excluye.
  - No es antagónica al límite, sino que está en estrecha relación que nada
    simple puede acabar.

La transgresión no es lo escandaloso ni lo subversivo.
  - No es potencia de lo negativo.
  - La transgresión no se opone ni se burla de nada: no busca sacudir la solidez
    de los fundamentos.
=> No es violencia en un mundo dividido —mundo ético— ni triunfo sobre los
   límites —mundo dialéctico o revolucionario—.

La transgresión es la afirmación del ser limitado.
  - Rastros en la filosofía contemporánea:
    - Kant y su distinción entre *nihil negativum* y *nihil privativum*.
    - Blanchot y su principio de constatación.
  - Afirmación que no afirma nada.
  - Llevar al límite los valores y las existencias en lugar de negarlas.
  - Ir al límite que define al ser.

Experiencia de Bataille: la transgresión abierta a un mundo brillante y siempre
afirmado, hundido en la contradicción.
  - Dialéctica, análisis o trascendentalismo no sirven para acceder a esta
    experiencia.
    - Nietzsche ayudó a visualizar esta imposibilidad.

«Filosofía del erotismo» de Bataille como experiencia de la cultura que va desde
Kant y Sade.
  - Experiencia de la finitud, del ser, del límite y de la transgesión.
  - No tiene su fundamento en el vocabulario.

Desierto en la filosofía: pérdida del lenguaje que le era históricamente 
natural.
  - No es fin de la filosofía sino recobrarse desde los límites.
  - El lenguaje que nace es desdialectizado, que sabe que no lo somos todo,
    no es totalitario, se esfuerza y fracasa.
  - Fin del filósofo como forma soberana y primera del lenguaje filosófico.
  - Experimentación de su pérdida hasta el límite.
=> Lenguaje de peñascos.

El lenguaje de peñascos es su repliegue a la interrogación de sus límites.
  - El ojo como prestigio obstinado.
    - La mirada lleva al límite, un vínculo entre el lenguaje y la muerte.

La necesidad no es reducible a la dialéctica de la producción del hombre que
trabaja.
  - La sexualidad hace un desplazamiento de la filosofía al ser hablante, 
    no del conocimiento.
    - Hundimiento de la experiencia filosófica en el lenguaje que obliga a
      pensar.
