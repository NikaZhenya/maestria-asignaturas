# 7 de agosto

Bataille no ha sido digerida en el ámbito académico.
  - Contrario a Foucault.
=> ¿Motivos?
  - No hay teoría en Bataille.
  - No hay doctrina.

Francia del siglo XX en adelante solo hay un camino para la filosofía.
  - Camino de alto rendimiento
=> Bataille no estudió filosofía.
  - Su obra filosófica es *sui generis*.

¿Problema en su obra?
  - La experiencia interior y cómo comunicarla.
=> El tema de la expresión.

Personal / subjetivo > discurso filosófico => ¿Cómo?

¿Cómo comunicarlo con el lenguaje?
Esto es el problema de la expresión / comunicalidad.
=> Todo lenguaje es una construcción social.

Bataille pertenece a dos mundos:
  1. Al del discurso filosófico.
  2. Al de la literatura.

El pensar es discursivo.
  El discurso es determinado.

Para que un discurso sea significativo:
  - Tiene que ser determinado.
  - Necesita comunicalidad / base social del lenguaje.
=> Para Bataille esto es solo **una** posibilidad humana.
  - La filosofía se equivoca al pensar que es **la** posibilidad.
  - La otra posibilidad es que el discurso pierde coherencia pero de forma voluntaria.
  => La disolución del saber implica la disolución del sujeto.

¿Cómo leer a Bataille?
  - Un libro ordinario tiene un acto esperado.
  - Los libros que no son así, no se sabe cómo leerlos.

Bataille fue leído en México por la generación de los cincuenta.
  - Salvador Elizondo.
  - Juan García Ponce.
  - Huberto Batis
=> Esencialmente una parte: el erotismo.
  - No le es personalmente fascinante a Serna.

Obra de Bataille:
  - 12 tomos.
  - 600~800 pp. por tomo.
  - Edición a partir 1970.
  - Obra desorganizada.

Corazón de su obra: *Summa ateológica*.
  - 3 libros:
    1. *La experiencia interior*.
    2. *El culpable*.
    3. *Sobre Nietzsche. Voluntad de suerte*.
  - Mal escrito en francés conscientemente.
  - Planes para otros 2 libros:
    4. *La pura felicidad*.
    5. *El no-saber*.
    => Serna tome el plan en *La oscuridad no miente*.

Sus novelas no son una expresión de sus ideas nucleares.

*La parte maldita* es la obra más estructurada.

Según Bataille su mayor aporte a la filosofía son sus trabajos sobre la risa.
  - Explosión humana de haber tocado el límite de lo posible.
  => Posible: todo lo que acaece.
  => Imposible: muerte.

Para Serna, el principal aporte fue sobre la lógica de la transgresión.

Autores que leyó de manera extensa:
  - Heidegger pero no heideggeriano [se desprendió].
  - Freud pero antifreudiano [se desprendió].
  - Marx.
  - Hegel pero no hegeliano por no haber dialéctica.
  - Nietzsche.

Dos maneras de apropiarse de un autor:
  1. Leyéndolo.
  2. Descubrimiento en la vida.

Bataille: trabajo => lo sagrado y lo profano.
  - No religioso.
  - No místico.
  - Sí la construcción del saber:
    * Saber se inscribe con lo profano.
    * Lo sagrado es lo que transgrede la vida.

Bataille tiene:
  - Inventiva.
  - Multidisciplinariedad.

¿Cómo ir más allá del trabajo y el saber?
  - No-saber como artificio linguístico.

# 21 de agosto

Narración de Serna sobre el sistema educativo francés.
  - Requerimiento de demasiadas credenciales.
    - P. ej.: un oficio requiere 8 años de formación.
  - Bachillerato + 3 años de estudio = licenciatura.
  - Bachillerato + 5 años de estudio = maestría.
  - Bachillerato + 7 años de estudio = doctorado.
=> Bataille no hizo este recorrido.
  - Estudió en la École Nationale des Chartes para ser archivista-paleógrafo.
  - Sin formación filosófica.
  - Sus estudios de filosofía, introducido por Shestov.
    - Platón    => Fundamental en su formación.
    - Hegel     => Interpretación antropológica de Kojève
                   (asistieron Breton y Lacan).
    - Heidegger => Terminó desencantado de él y de la filosofía por su
                   especialización del lenguaje.
    - Nietzsche => Principalmente la *Genealogía*.
    - Mauss     => Tema del don.
    - Durkheim  => Introducción a la religiosidad, que no religión: 
                   lo sagrado y lo profano.

Hegel es muy importante para Bataille y la filosofía en general.
  - Fin de la filosofía sistemática sin posibilidad de nada más.
  - Bataille lee su correspondencia y encuentra la incertidumbre en la que se
    haya Hegel.
    => Al final el espíritu absoluto está ausente, no hay dialéctica al final.

*Summa Atheologica* es una pretensión arquitectónica.
  - Influencia de Hegel.
  - No es construir un sistema, sino una estrategia epistemológica para
    demostrar que es una tomada de pelo.
    - Con el saber no se comprende el sentido de la vida.
  - Vocabulario teológico para desviarlo de su sentido originario.
    - Freud presente por el sentido de la perversión.
    - Nietzsche presente por la *Genealogía*.
  - Despliega una continua parodia.
    - La parodia busca mostrar una paradoja.
  - Construcción de un sistema para parodiarlo.

La experiencia interior.
  - No es mística porque no hay preparación.
  - No es revelación.
  - Es experiencia de la radical pérdida continua.
    => No hay fidelidad enla comunicación.
       - Tiene que ser presente pero la experiencia es pasado,
         el presente es vivencia.

# 28 de agosto

No asistí.

# 4 de septiembre

Repaso: elsaber es una forma de trabajo.
  - vs: Aristóteles, que entiende el saber como un apetito.
  - El saber siempre ha sido utilitario, para satisfacer las condiciones
    biológicas: no hay distinción entre el saber filosófico y el técnico.

El hombre, al producir objetos, introduce la discontinuidad.
  - La continuidad es esencialmente comunicación: se busca.

El defecto del saber es que se ha orientado a objetos, en lugar de verse como
una relación.

El mundo: una construcción ordenada a través de la mirada.
  - Concepción epistemológica del sujeto y el objeto como confrontación y
    representación de la realidad.

# 11 de septiembre

El saber crea un mundo de las cosas donde solo había indeferenciación,
indeterminación.
  - Cosa-objeto es discontinuo y se empalma a la discursividad de la
    cosa-sujeto.
  - Cosa-sujeto lo primero que hace son sucesiones: el mundo como suma de
    cosas, contruido a través del discurso.
=> Se duplica la cosa (cosa-objeto y cosa-sujeto).
  => El saber solo es posible como discurso que produce la forma de lo real.

El saber ya es trabajar de un modo específico.
  - Afán de someter a la naturaleza y a nosotros mismos a nuestro proyecto.

Consecuencias de la doble subordinación: la digestión es de la cosa-objeto y
de la cosa-sujeto.

El saber es proyecto.
  - Hipotecar el momento actual en pos de un futuro.

La experiencia interior no es experiencia mística, sino experiencia límite.
  - Escapar de la racionalidad.
  - Sin meta.
  - Transgresión.

# 18 de septiembre

*El caballo académico*, 1929
  - Lo otro (no igual a "el otro").
  - El caballo como ideal helénico de armonía.
  - Con los galos el caballo representa el frenesí.
  - Bataille lo llama "la noche humana".
=> Ambos son vertientes humanos.
  - ¿Por qué se ha ignorado eso otro? ¿Por qué se finge que lo único que existe es la inteligibilidad?

Heterología: la ciencia de lo que es completamente otro.
  - No puede someterse a cualquier forma de racionalidad.
  - Lo otro no puedo formar parte de ningún sistema, es lo inconmensurable.
  - Hace que los desechos sean asimilables al pensamiento.
  - No es una teoría de la otredad.
  - Es una teoría de la tensión entre el mundo homogéneo 
    y lo que nunca logra absorber del todo.

En el saber existe una tendencia hegemónica.
  - El saber es una cuestión política.
  - La heterología es la resistencia.
  - Es un ejercicio de exclusión y jerarquización.

# 25 de septiembre

No hubo clases porque Serna está enfermo.

# 2 de octubre

No hubo clases por toma de las instalaciones.

# 9 de octubre

Economistas clásicos:
  - Producción.
    => Condiciones materiales.
    => Riquezas.
    => Escacez.
    => Acumulación.

Bataille:
  - Sobreabundancia.
    => Gasto.

Hay una concepción antropológica en *La parte maldita*.
  - Energía excedente: es impersonal, compete a la vida en sí, no a las formas
    concretas de la vida.
    - Su límite es el mundo orgánico.
  - El objeto y el sujeto no se separan.

Bajo el supuesto de unmundo de carestía y la necesidad de acumulación, nace la
angustia.
  - Incandescencia: actitud conforme al universo.

Reducción de la teoría económica clásica = soberbia en el conocimiento e
ignorancia de un movimiento energético más general.

# 16 de octubre

No asistí.

# 23 de octubre

<!-- Entrega del trabajo máximo el 20 de noviembre; no más de 15 cuartillas -->

<!--Loeb, la mejor traducción de Platón al inglés -->

El desarrollo industrial como forma de absorber el excedente.
  - De la riqueza en la tenencia de la tierra al capital.
  - Incluye el excedente de la vida humana.

El gusto improductivo es un gran problema porque implica revertir el mundo
moderno.

El don es un gesto de poder concretado en ofrecer riquezas.
  - No como poder político, sino como verbo: quien puede.
  - No es subordinación o sacrificio de sí.

<!-- Si la edición es libre como dilapidación, no tiene fin, no puede tener fin.
  - Crítica a los ideales del *software* y cultura libre.
  - La retribucion no tiene fin solo es una muestra de fuerza, de desenfreno -->

# 30 de octubre

No asistí.

# 6 de noviembre

El sentimiento de una maldición permea a toda la cultura.
  - El hombre obra de manera inversa.
    - La justicia como un paleativo al ir en contra del movimiento general.
=> Condenado al fracaso.

La gran apuesta de la filosofía es construir un discurso lúcido.
  - Dolor que da un carácter de genuina existencia.

Consumición: gasto total.

# 13 de noviembre

No asistí.
