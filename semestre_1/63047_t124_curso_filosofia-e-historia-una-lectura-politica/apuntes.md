# 10 de agosto

No es un curso, sino un seminario.
  - 20 min alguien expone una lectura.
  - El resto de la clase es un díalogo entre todos.
  - La clase durará más de lo establecido.
=> Todos tienen que leer.

# 17 de agosto

Se comentó *Historia como sistema* de José Ortega y Gasset, a partir del
resumen hecho por Erick Sepúlveda. [Ver PDF](https://github.com/NikaZhenya/maestria-asignaturas/raw/master/semestre_1/63047_t124_curso_filosofia-e-historia-una-lectura-politica/otros/material-visto-clase/ortega_jose-historia_como_sistema.pdf).

# 24 de agosto

Parcialmente suspendido por lluvia.

Se empezó a comentar «Ensayos y notas de filosofía» de José Gaos, a partir
del resumen hecho por Gustavo Adrián Alvarado García. [Ver PDF](https://github.com/NikaZhenya/maestria-asignaturas/raw/master/semestre_1/63047_t124_curso_filosofia-e-historia-una-lectura-politica/otros/material-visto-clase/gaos_jose-ideas_de_la_filosofia.pdf).

# 31 de agosto

Se continúo los comentarios sobre José Gaos.

Discusión entre el carácter problemático del vínculo entre el autor y su obra
cuando va más allá del plano cognoscitivo (hermenéutica, historiografía,
pedagogía) ya que también es el fundamento nuclear del concepto de
«propiedad intelectual».

# 7 de septiembre

No asistí.

# 14 de septiembre

Se comentaron la introducción y los capítulos 5 y 6 de *Filosofía de la praxis* 
de Adolfo Sánchez Vázquez, a partir del resumen hecho por Moisés Rodríguez 
Rosales. [Ver PDF](https://github.com/NikaZhenya/maestria-asignaturas/raw/master/semestre_1/63047_t124_curso_filosofia-e-historia-una-lectura-politica/otros/material-visto-clase/sanchez_adolfo-filosofia_de_la_praxis-parte_1.pdf).

El poder no se tiene, se ejerce: es operativo.

# 21 de septiembre

No hubo clases por luto nacional.

# 5 de octubre

No vino Magallón, pero sí su suplente.

Se comentaron los capítulos 7, 8 y 10 de *Filosofía de la praxis* de Adolfo 
Sánchez Vázquez, a partir del resumen hecho por Monserrat Ríos. 
[Ver PDF (PENDIENTE)]().

Discusión sobre la validez del marco conceptual en donde se identifican tres
rasgos de la praxis creadora:
  - ¿Pueden estar presentes estos tres ragos en las sociedades capitalistas?
    - Problema del constante vínculo entre los rasgos positivos con la
      revolución y las negativas con el capitalismo.
=> Todas las praxis están presentes en el capitalismo con el fin de continuar
   reproduciéndose.

# 12 de octubre

Se comentó *El péndulo y la espiral* de Ramón Xirau, a partir de los resúmenes
hechos por Victor García ([Ver PDF](https://github.com/NikaZhenya/maestria-asignaturas/raw/master/semestre_1/63047_t124_curso_filosofia-e-historia-una-lectura-politica/otros/material-visto-clase/xirau_ramon-el_pendulo_y_la_espiral-victor.pdf)) y Jorge Corona ([Ver PDF](https://github.com/NikaZhenya/maestria-asignaturas/raw/master/semestre_1/63047_t124_curso_filosofia-e-historia-una-lectura-politica/otros/material-visto-clase/xirau_ramon-el_pendulo_y_la_espiral-jorge.pdf)).

Xirau es un hombre que sorprende.
  - Muy controvertido.
  - Demasiado erudito.
  - Políglota.

El progreso es un problema ético.

# 19 de octubre

Se comentaron la introducción y la tercera parte de *La arqueología del saber* 
de Michel Foucault, a partir del resumen hecho por mi parte. 
[Ver PDF](https://github.com/NikaZhenya/maestria-asignaturas/raw/master/semestre_1/63047_t124_curso_filosofia-e-historia-una-lectura-politica/otros/material-visto-clase/foucault_michel-la_arqueologia_del_saber.pdf).

# 26 de octubre

Se comentaron los capítulos III, IV y VII de  *Ordo Analogie* de Mauricio 
Beuchot, a partir del resumen hecho por Cedith Serrano. [Ver PDF (PENDIENTE)]().

# 9 de noviembre

Se empezó a comentar la primera parte de *El sublime objeto de la ideología* de 
Slavoj Zizek, a partir del resumen hecho por Gustavo Adrián Alvarado García. 
[Ver PDF](https://github.com/NikaZhenya/maestria-asignaturas/raw/master/semestre_1/63047_t124_curso_filosofia-e-historia-una-lectura-politica/otros/material-visto-clase/zizek_slavoj-el_sublime_objeto_de_la_ideologia.pdf).

El deseo es necesario para poder vivir.
  - En el capitalismo es convertido en una necesidad mediada con el consumo.

<!-- Trabajo final: no más de 10 páginas para el ~19 de diciembre -->

# 16 de noviembre

No hubo clases porque Magallón se fue a San Luis Potosí.

# 23 de noviembre

La filosofía por la filosofía no existe.
  - No se puede hacer a un lado la historia.

<!-- ¿Se puede hacer a un lado la información? -->

El sistema no logra control.

<!-- Por eso un afán de control -->

# 30 de noviembre

Se comentaron la introducción y el capítulo I de  *Filosogía de la historia americana* 
de Leopoldo Zea, a partir del resumen hecho por Poncho. [Ver PDF (PENDIENTE)]().

Magallón no asistió, pero vino su adjunto.

<!-- ¿Proyecto liberador del *software* libre como autocolonización? -->

<!-- El problema de la divulgación de la filosofía en un sistema de información
     cerrada -->
