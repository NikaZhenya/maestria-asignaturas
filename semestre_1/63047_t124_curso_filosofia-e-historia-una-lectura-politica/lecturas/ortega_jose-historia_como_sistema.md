# *Historia como sistema*, José Ortega y Gasset

## I

Vida humana = realidad radical.
  - Nos encontramos de pronto y sin saber cómo.
  - Nos es dada pero no hecha.
  => Es quehacer.
  - Forzados a hacer algo => decidir.
  - Necesidad de convicciones sobre las cosas.
    => La estructura de la vida depende de creencias.
=> Transformaciones decisivas en la humanidad son:
  - Cambios         >
  - Intensificación > de creencias. 
  - Debilitación    >

«Repertorio» de creencias:
  - Una pluralidad sin articulación lógica, sino vital y jerarquizada.
    - Incongruentes, contradictorias o inconexas.

La creencia es la idea que se piensa y se cree.
  - Operación intelectual.
  - Función que orienta la conducta.

La estructura de las creencias permite entender la vida propia y ajena.
  - El diagnóstico empieza fijando su creencia fundamental.                    <
    - Comparar creencias, entre más habrá mayor precisión.
  
## II

Se ha alterado la convicción fundamental que iba del siglo XVI al XX: 
la fe en la razón.
  - Descartes como gallo del racionalismo.
  - Sin considerar lo divino, para el hombre no hay nada irresoluble.
  - Pensar como *ratio* (razón).
  - Mundo dual realidad-pensamiento.
  - Mundo como estructura racional.
  - Mundo coincide con el intelecto humano.                                    <

Dos tipos de creencia:                                                         <
  - Fe viva: cuando basta para vivir.
  - Fe inerte: cuando está pero ya no actúa eficazmente, se olvida.
=> La fe viva palidece hasta ser inerte.
  - El Renacimiento como fe revivida, pero ya en la razón.

## III

Las ideas se tienen, las creencias se es:
  - Se vive en ellas, no suelen pensarse.
  - Se piensa lo que está en cuestión.
=> Las creencias se son como individuos y otras como colectivo.
  - Puede haber coincidencia.
  - La creencia colectiva no depende de que sea o no aceptada, sino de su
    imposición que nos obliga a tomarla en cuenta.
  => Vigencia (analogía a las leyes naturales).
    => Dogma social: en el siglo XX es la fe en la ciencia.

## IV

La ciencia ha pasado de ser fe viva a inerte.
  - La ciencia está en peligro.
  - Rigurosamente es ciencia naturalista.
    - Ciencia fisicomatemática.
    - Ciencia biológica.
  - El peligro es porque la ciencia ha triunfado en la naturaleza, pero ahora
    esta se percibe como solo una dimensión humana.
    - Hay un déficit que ayuda en su desazón.
  => Lo que ha fracasado es la retórica, no la física.
    - La insistencia de tratarlo todo con su método.
      => calendas griegas.
    => Base para la idea de la vida como realidad radical y el conocimiento
       como función interna a la vida.
        - La vida es prisa, por lo que la vigencia tiene que ser su método.
        - La verdad no es un vago mañana.

La fe en la razón física generó:
  - Una vida sin cimientos.
  - Una existencia a cargo de la posteridad.
  - Un armazón superficial de la civilización.
  - Un hombre sin verdades propias.
=> Ante los problemas humanos, no sabe qué decir => fe inerte.
  - Apertura a la razón vital e histórica.

## V

La naturaleza es una cosa compuesta de cosas.
  - Lo común es que son, tienen un ser.
    - Con consistencia fija y dada.
  => La ciencia consiste en descubrir esto, su «naturaleza».

Cuando la ciencia naturalista se ocupa del hombre, busca su naturaleza:
  - Cosa corpórea.
  - Cosa psíquica.
=> No ha aclarado lo humano, lo que se llama vida.
  - Motivo de la decadencia en la fe de la razón.

El hombre no es unacosa => no tiene naturaleza.
  - Convicción por 300 años de fracaso.
  - Cambiar el cuadrante.

## VI

Otra forma de ciencia distinta a las naturales:
  - Ciencias del espíritu (ciencias de la cultura).
    - Intento larvado de hacer lo mismo.

*Geist* (espíritu) como aquello que pretende oponerse a la naturaleza.
  - Ensayo del idealismo alemán y el positivismo de Comte.
=> Interpretación violenta, arbitraria y fallida.
  - Sustitución conceptual con atributos antagónicos.                          <
    - La doctrina sobre el ser no fue tocada.
  => Ontología tradicional: *natura* como principio de la *res*.
    - *Natura* es de simiento griega que quiere decir «ley».
      - No es igual en Aristóteles que en el siglo XX, pero algo permaneció
        invariable.
    => El europeo como heredero del griego.
        - Esclavos del destino helénico.
        - Prisioneros de la ontología griega.

Desde Parménides el ser de una cosa es consistencia fija y estática;           <
es decir, el ente => ser *ya* lo que es => ser-siempre-lo-mismo.
  - vs movimiento.

Aristóteles opta por una posición intermedia:
  - Búsqueda en la cosa lo que en su movimiento permanece.
    - «Naturaleza» como lo realque parece ocultarse.
  - Puente entre el ser y su aparente multiplicidad.
    - El eleatismo pervive al quedarse el ser como estabilidad profunda.

Del *natura* aristotélico a la ley de los fenómenos acontece una depuración.
  - La invariabilidad de la ley como declaración del ser fijo eléatico.

Arbitrariedad de Parménides:
  - Poner lo idéntico como condición de lo real.
    - No hay distinción entre lo que pertenece al intelecto y
      lo que es del objeto.

Hasta Kant esta la distinción que implica una «segunda navegación»:
  1. Pensar sobre lo real.
  2. Regresar a lo pensado.
  3. Restar lo que es forma intelectual.
  4. Dejar solo la intuición de lo real.
  => ¬(P·Q), P, entonces ¬Q.
=> Desintelectualizar lo real para serles fieles.
  - Eleatismo = intelectualización del ser.                                    <
  - El naturalismo en su raíz es intelectualismo.
  => Lo real no es lógico, solo el pensamiento racional.

El espíritu es identidad, *res*, cosa; tiene consistencia estática.
  - Su movimiento es ficción.
  - La cosa no es cosa por su espacialidad y materialidad,
    sino por su consistencia e identidad.
  - La cosa son ideas que salen de la cabeza y son tomadas como realidades.
=> La naturaleza es una interpretación de lo que se encuentra en la vida.
  - Liberarse del naturalismo e ir a la realidad radical, a la vida.
  - No aceptar imperativos de la ontología tradicional.

## VII

El hombre no es su cuerpo ni su alma ni cosa alguna => el hombre es drama.

Las cosas son interpretaciones de lo que se encuentra.
  - El hombre no encuentra cosas, las supone.
    - Se encuentra con facilidades o dificultades para existir.
  => La vida es quehacer
      - Ser no como cosa, sino como ser indigente.
      - El hombre se hace a sí mismo y determinar lo que va a ser.
        => programa vital.

Sobre las posibilidades de ser:
  1. No son regaladas, sino inventadas.
    - Lo único dado es la circunstancia.
    - El hombre es novelista de sí mismo como original o plagiario.            <
  2. Se tiene elección, por ende, se está forzado a ser libre.
    - Libertad no es una actividad, sino carecer de identidad constitutiva,
      no poder ser determinado.
    - Lo único fijo es la inestabilidad.

Necesidad de un concepto no-eleático del ser => Heráclito.
  - En cada instante se abren posibilidades limitadas.
  - La vida humana no es cambio «accidental» sino «sustancial».
  - La vida es «drama», el sujeto no es «cosa» y la «sustancia»
    sería su argumento.
  - Un ser viviente que anula su identidad.

## VIII

Lo que hacemos sido actúa negativamente sobre lo que podemos ser.
  - La vida es experiencia de vida.
  - El hombre está forzadoa avanzar sobre sí mismo.
  => No solo individual sino también colectiva.
      - La sociedad es pasado donde la instauracióon de un nuevo uso depende
        de lo que ha sido.
      - «tardígrada».
      - Pasado como parte del presente.

El hombre no es sino que va siendo => El hombre vive.
  - Paralelismo:
    - Hombre como ser => razón física-matemática.
    - Hombre como vivo => razón narrativa-histórica.

Creencia en el ser: la constitución de un personaje estático que responde
a las circunstancias mediante su realización.
  - Se muestra como insuficiente: va siendo y des-siendo; es decir, viviendo.
=> Dialéctica de las experiencias.
    - Solo discernible sobrelos hechos pasados.
      => Historia.

El límite es el pasado.
  - Las experiencias estrechan el futuro.
  - No se sabe lo que se va a ser, pero sí lo que no va a ser.
=> El hombre no tiene naturaleza sino historia.
    - El ser varía y crece => «progresa».
      - vs: creencia *a priori* donde progreso = hacia lo mejor.

La mudanza no es lamentable sino un privilegio ontológico.
  - Progreso es aprovechar lo anterior.

La historia es un sistema de experiencias humanas.
  - Cada término histórico ha de estar fijado en función de toda la historia.
  - La historia como ciencia sistemática de la realidad radical que es la vida.

## IX

El hombre necesita una revelación.
  - La razón (física) ya lo fue.

Las ideas tienen dos papeles en la vida:
  1. Como mera idea: sin importar su rigor, son invenciones.
    => mero intelecto utilitarista.
  2. Como presencia absoluta: como patética trascendencia de una revelación.

La razón era una convicción radicalsobre el mundo.
  - Ahora es algo estrecho (como mera idea).
  - Necesidad de ser de nuevo revelación.
  => La presión de una trascendencia hace ver la realidad de lo que queday
     no se había reparado.
      - Cartesianismo de la vida.
      - Encuentro consigo mismo como historia.
        => Obligación a ocuparse de su pasado porque *no tiene otra cosa*.
            - Pasar a la historia como razón histórica.

La razón histórica es rigurosa:                                                <
  - No acepta nada como mero hecho.
  - Ve cómo se hace el hecho.
  - No es buscar en la historia una sustancia radical.
  - Es encontrar en la historia su autóctona razón.
  - No es una razón extrahistórica cumpliéndose en la historia.
  - Es la historia que ha constituido su razón.
  - No es irracionalismo.
